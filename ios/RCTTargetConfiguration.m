//
//  RCTTargetConfiguration.m
//  appdasorte
//
//  Created by Diego Bezerra on 22/03/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "RCTTargetConfiguration.h"
#import <React/RCTLog.h>

@implementation RCTTargetConfiguration

RCT_EXPORT_MODULE(TargetConfiguration);

RCT_REMAP_METHOD(getTargetConfiguration, resolver: (RCTPromiseResolveBlock)resolve
     rejecter:(RCTPromiseRejectBlock)reject) {
  
  NSString* target = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"TargetName"];
  resolve(target);
}

@end
