//
//  RCTTargetConfiguration.h
//  appdasorte
//
//  Created by Diego Bezerra on 22/03/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCTTargetConfiguration : NSObject <RCTBridgeModule>

@end
