import React, { Component } from 'react'
import { Text, TextInput, YellowBox, Linking, View, Button } from 'react-native'
import Main from './src/components/screens/main/main'
import OneSignal from 'react-native-onesignal'
import UserController from './src/data/user/userController'
import BadgeNotificationsController from './src/data/badgeNotifications/badgeNotificationsController'
import Application, { FABRIC_NOTIFICATION } from './src/appConfig/application'

const NotificationType = {
  CONFIRMED_PURCHASE: 1,
  PENDING_PURCHASE: 2,
  CANCELED_PURCHASE: 3,
  properties: {
    1: { routeKey: 'MyCertificatates' },
    2: { routeKey: 'PendingCertificatates' },
    3: { routeKey: 'CanceledCertificatates' },
  }
}

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
Text.defaultProps.maxFontSizeMultiplier = 1;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.maxFontSizeMultiplier = 1;

YellowBox.ignoreWarnings(['[RCTRootView]']);

export default class App extends Component {

  state = {
    loaded: false
  }

  constructor() {
    super();
    this.loadUser();
    this.init();
  }

  componentDidMount() {
  }

  async loadUser() {
    await Application.instance.loadUserData();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  async init() {
    let config = await Application.instance.loadConfiguration();
    await Application.instance.loadParameters();
    setTimeout(() => {
      this.setState({ loaded: true });
    }, 300);
    OneSignal.init(config.oneSignalId, { kOSSettingsKeyInAppLaunchURL: false });
    OneSignal.addEventListener('received', this.onReceived.bind(this));
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    OneSignal.inFocusDisplaying(1);
  }

  onReceived(notification) {
    console.log('onReceived');
    try {

      let logData = {
        isAppInFocus: notification.isAppInFocus,
        notificationType: notification.payload.additionalData ? notification.payload.additionalData.notificationType : null,
        notificationId: notification.payload.notificationId,
        priority: notification.payload.priority,
        title: notification.payload.title,
        body: notification.payload.body,
      }

      Application.logNotification(FABRIC_NOTIFICATION.RECEIVED, logData);
      let notificationType = this.getNotificationType(notification);
      if (notificationType) {
        let routeKey = NotificationType.properties[notificationType].routeKey;
        BadgeNotificationsController.storeBadgeNotificationsData(routeKey);
      }

    } catch (error) {
      //alert('error');
      console.log(error);
      Application.logError(error);
    }
  }

  async onOpened(openResult) {

    console.log('openResult');
    let notification = openResult.notification;
    let notificationType = this.getNotificationType(notification);
    if (notificationType) {
      let logData = this.generateLogData(openResult.notification);
      Application.logNotification(FABRIC_NOTIFICATION.OPENED, logData);
      let userData = await UserController.retrieveUsersData();
      if (!userData) {
        return;
      }
      let routeKey = NotificationType.properties[notificationType].routeKey;

      if (!notification.isAppInFocus) {
        BadgeNotificationsController.setNotificationRouteAction(routeKey);
      }
    }
  }

  async onIds(device) {
    console.log(JSON.stringify(device))
    try {
      Application.logNotification(FABRIC_NOTIFICATION.ON_ID, { userId: device.userId });
      await UserController.storePlayerId(device.userId);
      let userData = Application.instance.userData;
      if (userData) {
        await UserController.updatePlayerId(userData.usuario.id, device.userId, userData.token);
      }
    } catch (error) {
      console.log(error)
      if (error && error.code === Application.instance.TokenExpiredCode) {
        Application.instance.removeUserData();
      } else {
        console.log(error);
        Application.logError(error);
      }
    }
  }

  generateLogData(notification) {

    let userData = {};
    if (Application.instance.__userData && Application.instance.__userData.usuario) {
      userData = Application.instance.__userData.usuario;
    }

    return Object.assign(userData, {
      isAppInFocus: notification.isAppInFocus,
      notificationType: notification.payload.additionalData ? notification.payload.additionalData.notificationType : null,
      notificationId: notification.payload.notificationId,
      priority: notification.payload.priority,
      title: notification.payload.title,
      body: notification.payload.body,
    });
  }

  getNotificationType(notification) {
    if (notification.payload && notification.payload.additionalData && notification.payload.additionalData.notificationType) {
      return notification.payload.additionalData.notificationType;
    }
    return null;
  }

  render() {
    if (!this.state.loaded) {
      return null;
    }
    return (
      // <View style={{ flex: 1 }}>
      //   <Main />
      //   <Button title={'add'} onPress={() => {
      //     BadgeNotificationsController.storeBadgeNotificationsData('MyCertificatates');
      //   }} />
      // </View>
      <Main />
    );
  }
}