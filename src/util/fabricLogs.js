//import { Crashlytics, Answers } from 'react-native-fabric'

const CURRENCY_BR = 'BRL';
export const FABRIC_LOGIN_TYPES = {
    NATIVE: 'native'
}
export const FABRIC_NOTIFICATION_TYPE = {
    RECEIVED: 'Received',
    OPENED: 'Opened',
    ON_ID: 'OnId',

}

export default class FabricLogs {

    static crashTest() {
        //Crashlytics.crash();
    }

    static logError(error, userData, platform) {
        // this.setUser(userData);
        // if (platform.OS === 'ios') {
        //     Crashlytics.recordError(error.message);
        // } else {
        //     Crashlytics.logException(error.message);
        // }
    }

    static logPurchase(total, success, itemType, itemName, itemId, data) {
        // Answers.logPurchase(
        //     total,
        //     CURRENCY_BR,
        //     success,
        //     itemType,
        //     itemName,
        //     itemId,
        //     data);
    }

    static logContentView(name, userData) {
        //Answers.logContentView(name, 'screen', name, userData);
    }

    static logAddToCart(value, itemType, itemName, itemId, userData) {
        // Answers.logAddToCart(
        //     value,
        //     CURRENCY_BR,
        //     itemType,
        //     itemName,
        //     itemId,
        //     userData);
    }

    static logSignUp(value) {
        //Answers.logSignUp(FABRIC_LOGIN_TYPES.NATIVE, value);
    }

    static logLogin(value, userData) {
        //Answers.logLogin(FABRIC_LOGIN_TYPES.NATIVE, value, userData);
    }

    static logNotification(type, data) {
        //Answers.logCustom(type, data);
    }

    static setUser(userData) {
        // if (userData && userData.usuario) {
        //     Crashlytics.setUserName(userData.usuario.nome);
        //     Crashlytics.setUserEmail(userData.usuario.email);
        //     Crashlytics.setUserIdentifier(userData.usuario.id);
        // }
    }
}