import { Alert } from 'react-native'

const alertTitle = 'Alerta';
export const InputEnum = {
    NAME: { id: 0, key: 'nome' },
    CPF: { id: 1, key: 'cpf' },
    EMAIL: { id: 2, key: 'email' },
    BIRTH_DATE: { id: 3, key: 'data_nascimento' },
    ZIP_CODE: { id: 4, key: 'cep' },
    PHONE: { id: 5, key: 'telefone' },
    PASSWORD: { id: 6, key: 'senha' },    
    PASSWORD_CONFIRMATION: { id: 7, key: 'senha_confirmacao' },
    OLD_PASSWORD: { id: 8, key: 'senha_antiga' },
}

export default class Validations {

    static removeCharacters(str) {
        str = this.removeCharacter(str, '/');
        str = this.removeCharacter(str, '(');
        str = this.removeCharacter(str, ')');
        str = this.removeCharacter(str, '-');
        return str.trim();
    }

    static removeCharacter(str, cha) {
        if (str.indexOf(cha) !== -1) {
            str = str.replace(cha, '');
            str = this.removeCharacter(str, cha);
        }
        return str;
    }

    static validateEmailString(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;        
        return re.test(String(email).trim().toLowerCase());
    }

    static validateString(string) {
        return string !== null && string !== undefined && string.trim().length !== 0;
    }

    static validateName(name) {
        if (!this.validateString(name)) {
            this.showAlert(alertTitle, 'Preencha o seu nome');
            return false;
        }
        return true;
    }

    static validateCpf(cpfComponent) {
        if (!cpfComponent.isValid()) {
            this.showAlert(alertTitle, 'CPF inválido.');
            return false;
        }
        return true;
    }

    static validateEmail(email) {
        if (!this.validateEmailString(email)) {
            this.showAlert(alertTitle, 'E-mail inválido.');
            return false;
        }
        return true;
    }

    static validateBirthDate(birthDateComponent) {
        if (!birthDateComponent.isValid()) {
            this.showAlert(alertTitle, 'Data de nascimento inválida.');
            return false;
        }
        return true;
    }

    static validateZipCode(zipCodeComponent) {
        if (!this.validateString(zipCodeComponent.getRawValue()) || !zipCodeComponent.isValid()) {
            this.showAlert(alertTitle, 'CEP inválido.');
            return false;
        }
        return true;
    }

    static validatePhoneNumber(phoneNumberComponent) {
        if (!phoneNumberComponent.isValid()) {
            this.showAlert(alertTitle, 'Telefone inválido.');
            return false;
        }
        return true;
    }

    static validatePasswordConfirmation(confirmation, password, inputText) {
        if (confirmation !== password) {
            this.showAlert(alertTitle, inputText + ' e Senha de Confirmação não coincidem.');
            return false;
        }
        return true;
    }

    static validateOldPassword(oldPassword) {
        if (!this.validateString(oldPassword)) {
            this.showAlert(alertTitle, 'Preencha sua senha antiga.');
            return false;
        }
        return true;
    }

    static validateNewPassword(newPassword) {
        if (!this.validateString(newPassword)) {
            this.showAlert(alertTitle, 'Preencha sua nova senha.');
            return false;
        }
        return true;
    }

    static validateConfirmation(confirmation) {
        if (!this.validateString(confirmation)) {
            this.showAlert(alertTitle, 'Confirme sua senha.');
            return false;
        }
        return true;
    }

    static validatePassword(password, inputText) {
        if (!this.validateString(password)) {
            this.showAlert(alertTitle, 'Preencha sua ' + inputText + '.');
            return false;
        }
        return true;
    }

    static validateNewPassword(password) {
        if (!this.validateString(password)) {
            this.showAlert(alertTitle, 'Preencha a nova senha.');
            return false;
        }
        return true;
    }

    static showAlert(title, msg) {
        Alert.alert(title, msg);
    }

    static validateByKey(key, ref, isEdit) {
        isEdit = isEdit ? isEdit : false;
        let val = ref.props.value;
        switch (key) {
            case InputEnum.NAME.key:
                return this.validateName(val);
            case InputEnum.CPF.key:
                let cpf = ref;
                return this.validateCpf(cpf);
            case InputEnum.EMAIL.key:
                return this.validateEmail(val);
            case InputEnum.BIRTH_DATE.key:
                let birthDate = ref;
                return this.validateBirthDate(birthDate);
            case InputEnum.ZIP_CODE.key:
                let zipCode = ref;
                return this.validateZipCode(zipCode);
            case InputEnum.PHONE.key:
                let phoneNumber = ref;
                return this.validatePhoneNumber(phoneNumber);
            case InputEnum.PASSWORD.key:
                return this.validatePassword(val, isEdit ? 'nova senha' : 'senha');
            case InputEnum.PASSWORD_CONFIRMATION.key:
                return this.validateConfirmation(val);
            case InputEnum.OLD_PASSWORD.key:
                return this.validateOldPassword(val);

            default:
                break;
        }
    }
}