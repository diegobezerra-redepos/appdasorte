
import ServiceApi from '../../services/serviceApi'

export default class MapController {

    static async getSellPointsData(companyId, latitude, longitude, distance, geocodeKey) {

        try {            
            let result = await ServiceApi.instance.getSellPointsData(companyId, latitude, longitude, distance);
            let data = result.data;

            await Promise.all(data.dados.map(async obj => {
                let addresses = await this.addressesByLatLng(obj.GPS_LATITUDE, obj.GPS_LONGITUDE, geocodeKey);
                console.log(addresses);
                obj.ADDRESS = addresses.data.results[0];
            }));            
            return data;
        } catch (error) {
            throw error;
        }
    }

    static async addressesByLatLng(latitude, longitude, geocodeKey) {
        try {
            let addresses = await ServiceApi.instance.addressesByLatLng(latitude, longitude, geocodeKey);
            return addresses;
        } catch (error) {
            throw error;
        }
    }
}