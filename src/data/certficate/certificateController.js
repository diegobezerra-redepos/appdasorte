import ServiceApi from '../../services/serviceApi'
import CertificateStorage from './certificateStorage'
import FabricLogs from '../../util/fabricLogs'
import { Platform } from 'react-native'

const CERTIFICATE_REQUEST_TIME = 60 * 1000 * 60;

export default class CertificatesController {

    static async getNewCertificatesFromApi(params, userData) {

        if (!params || !params.info || !params.info.edicao_atual) {
            throw 'No editions data';
        }

        let token = userData ? userData.token : null;
        let editionId = params.info.edicao_atual.id;

        return ServiceApi.instance.getNewCertificates(token, editionId)
            .then((result) => {
                if (result.data.success === 'true') {
                    let data = result.data.data;
                    this.storeCertificatesData(data);
                    return result;
                } else {
                    throw 'No data'
                }
            }).catch((error) => {
                console.log(error);
                throw error;
            });
    }

    static async addSelectedCertificate(data, value, base) {
        let storedData = await this.retrieveSeletedCertificates();
        if (!storedData) {
            storedData = {
                total: 0,
                length: 0,
                certificates: {}
            };
        }
        let key = '_' + data.sequencial;
        data.value = value;
        storedData.certificates[key] = data;
        storedData.total += parseFloat(value);
        storedData.length++;

        await this.storeSeletedCertificate(storedData);
        return { count: storedData.length++, value: value, itemId: data.sequencial };
    }

    static async deleteSelectedCerticatesData(sequencial) {
        let storedData = await this.retrieveSeletedCertificates();
        if (storedData && storedData.certificates) {
            let key = '_' + sequencial;
            let value = storedData.certificates[key].value;
            delete storedData.certificates[key];
            storedData.total -= parseFloat(value);
            storedData.length--;
            await this.storeSeletedCertificate(storedData);

            return { success: true, total: storedData.total, count: storedData.length };
        }

        return { success: false, total: 0 };
    }

    static async sellInitialization(userData, params, base) {
        let data = await this.createSellData(userData, params);
        if (data) {
            return ServiceApi.instance.sellInitialization(userData.token, data).then((result) => {
                let success = result.data.success === 'true';
                let itemId = data.id;
                //let itemId = success ? result.data.data : null;
                this.logPurchase(data, base, success, itemId, userData);
                return result;
            }).catch((error) => {
                this.logPurchase(data, base, false);
                throw error;
            });
        } else {
            throw new Error('Nenhum certificado no carrinho.');
        }
    }

    static logPurchase(data, base, success, itemId, userData) {
        let itemName = 'Certificado';
        let itemType = itemName + ' ' + base;
        let fabricData = Object.assign({}, userData, data);
        fabricData.qtd = fabricData.qtd.toString();
        fabricData.tipo = fabricData.tipo.toString();
        fabricData.canalVenda = fabricData.canalVenda.toString();
        FabricLogs.logPurchase(data.valor, success, itemType, itemName, itemId, fabricData);
    }

    static async createSellData(userData, params) {
        let storedData = await this.retrieveSeletedCertificates();
        if (storedData && storedData.length > 0) {
            let data = {};
            data.id = userData.usuario.id;
            data.cpf = userData.usuario.cpf;
            data.nome = userData.usuario.nome;
            data.telefone = userData.usuario.telefone;
            data.nascimento = userData.usuario.data_nascimento;
            data.tipo = params.info.edicao_atual.produtos[0].tipo;
            data.valor = storedData.total.toString();
            data.prodId = params.info.edicao_atual.produtos[0].id;
            data.idEdicao = params.info.edicao_atual.id;
            data.idEmpresa = params.info.empresa.id_empresa;
            data.prodDescricao = params.info.edicao_atual.produtos[0].nome_curto;
            data.idTipoLoteria = params.info.empresa.id_tipo_loteria;
            data.canalVenda = Platform.OS === 'android' ? 3 : 4;
            data.certificados = [];
            data.qtd = storedData.length;
            data.certificados = new Array();
            if (userData.promoCode) {
                data.codigoPromocional = userData.promoCode;
            }

            let certificates = Object.values(storedData.certificates);
            certificates.forEach(cert => {
                let certData = {};
                certData.sequencial = cert.sequencial;
                certData.bolas = cert.dezenas;
                certData.itens = cert.itens.map((item) => { return { sequencial: parseInt(item.sequencial), bolas: item.dezenas } });
                certData.valor = cert.value;
                certData.idEdicao = params.info.edicao_atual.id;
                data.certificados.push(certData);
            });

            console.log(JSON.stringify(data));
            return data;
        }

        return null;
    }

    static async storeSeletedCertificate(data) {
        return CertificateStorage.storeSeletedCertificate(data);
    }

    static async retrieveSeletedCertificates() {
        return CertificateStorage.retrieveSeletedCertificates();
    }

    static async retrieveSeletedCertificatesCount() {
        return CertificateStorage.retrieveSeletedCertificates()
            .then((result) => {
                if (result) {
                    return result.length;
                }
                return 0;
            }).catch((error) => {
                throw error;
            });
    }

    static async storeCertificatesData(data) {
        await CertificateStorage.storeDate(new Date());
        return CertificateStorage.storeCertificatesData(data);
    }

    static async retrieveCertificatesData() {
        return CertificateStorage.retrieveCertificatesData();
    }
}