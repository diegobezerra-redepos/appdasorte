
import Storage, { storageKeys } from '../storage'

export default class ParametersStorage {

    static storeParametersData = async (data) => {
        return Storage.storeData(data, storageKeys.PARAMETERS_KEY);
    }

    static retrieveParametersData = async () => {
        return Storage.retrieveData(storageKeys.PARAMETERS_KEY);
    }

    static removeAllData() {
        Storage.removeAllData();
    }
}