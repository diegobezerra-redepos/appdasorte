import ParametersStorage from './parametersStorage';
import ServiceApi from '../../services/serviceApi';

export default class ParametersController {

    static async storeParametersData(data) {
        return ParametersStorage.storeParametersData(data);
    }

    static async retrieveParametersData(base) {
        return this.retrieveParametersDataApi(base);
    }

    static async removeAllData() {
        return ParametersStorage.removeAllData();
    }

    static async getEditionId() {
        let params = await ParametersStorage.retrieveParametersData();
        return params && params.info.edicao_atual ? params.info.edicao_atual.id : null;
    }

    static async retrieveParametersDataApi(base) {
        return ServiceApi.instance.requestParameters(base)
            .then((result) => {
                let params = result;
                params = params.data.data;
                ParametersStorage.storeParametersData(params);
                return params;
            }).catch((error) => {
                console.log(error);
            })
    }
}