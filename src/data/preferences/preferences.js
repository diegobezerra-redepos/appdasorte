

import Storage from '../storage'

const TERMS_ACCEPTED_KEY = 'termsAcceptedKey';
const TERMS_TIMESTAMP_KEY = 'termsTimeStampKey';

export default class Preferences {

    static async isTermsAccepeted(paramTime) {
        if (paramTime) {
            let getTime = await this.getTermsTimeStamp();
            if (getTime === null) {
                getTime = 0;
            }            
            if (getTime < paramTime) {
                await this.setTermsAccepted(false);
                await this.setTermsTimeStamp(paramTime);
            }
        }
        let termsAccepted = await Storage.retrieveData(TERMS_ACCEPTED_KEY);
        return termsAccepted === 'true';
    }

    static async setTermsAccepted(termsAccepted) {
        return Storage.storeData(termsAccepted.toString(), TERMS_ACCEPTED_KEY);
    }

    static async getTermsTimeStamp() {
        return Storage.retrieveData(TERMS_TIMESTAMP_KEY);
    }

    static async setTermsTimeStamp(time) {
        return Storage.storeData(time.toString(), TERMS_TIMESTAMP_KEY);
    }
}