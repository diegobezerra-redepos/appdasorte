
import AsyncStorage from '@react-native-community/async-storage';

//showAsyncStorageContentInDev();

export default class Storage {

    static storeData = async (data, key) => {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(data));
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    static retrieveData = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                return JSON.parse(value);
            }
            return value;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    static removeData = async (key) => {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        }
        catch (error) {
            throw error;
        }
    }

    static removeAllData() {
        try {            
            let keys = [storageKeys.CERTIFICATES_API_KEY, storageKeys.CERTIFICATES_SELECTED_KEY, storageKeys.CERTIFICATES_DATE_KEY];
            AsyncStorage.multiRemove(keys);
        } catch (error) {
            throw error;
        }
    }
}

export const storageKeys = {
    USER_KEY: 'userData',
    USER_PLAYER_ID_KEY: 'playerId',
    PARAMETERS_KEY: 'parameters',
    CERTIFICATES_API_KEY: 'certificates_api',
    CERTIFICATES_SELECTED_KEY: 'certificates_selected',
    CERTIFICATES_DATE_KEY: 'certificates_date',
    NOTIFICATIONS_KEY: 'notifications_data_key',
};