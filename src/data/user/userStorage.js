
import Storage, { storageKeys } from '../storage'

export default class UserStorage {

    static storeUserData = async (data) => {
        return Storage.storeData(data, storageKeys.USER_KEY);
    }

    static retrieveUserData = () => {
        return Storage.retrieveData(storageKeys.USER_KEY);
    }    

    static removeUserData = () => {
        return Storage.removeData(storageKeys.USER_KEY);
    }

    static storePlayerId = async (playerId) => {
        return Storage.storeData(playerId, storageKeys.USER_PLAYER_ID_KEY);
    }

     static retrievePlayerId = async () => {
        return Storage.retrieveData(storageKeys.USER_PLAYER_ID_KEY);
    }
}