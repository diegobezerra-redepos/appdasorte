import ServiceApi from "../../services/serviceApi";

export default class TransactionsController {

    static async getPendingTransactions(token, userId, editionId) {
        return ServiceApi.instance.getPendingTransactions(token, userId, editionId);
    }

    static async getConfirmedTransactions(token, userId, editionId) {
        return ServiceApi.instance.getConfirmedTransactions(token, userId, editionId);
    }

    static async getCanceledTransactions(token, userId, editionId) {
        return ServiceApi.instance.getCanceledTransactions(token, userId, editionId);
    }
}