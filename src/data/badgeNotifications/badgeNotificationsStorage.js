
import Storage, { storageKeys } from '../storage'

export default class BadgeNotificationsStorage {

    static storeBadgeNotificationsData = async (data) => {
        return Storage.storeData(data, storageKeys.NOTIFICATIONS_KEY);
    }

    static retrieveBadgeNotificationsData = async () => {
        return Storage.retrieveData(storageKeys.NOTIFICATIONS_KEY);
    }    
}