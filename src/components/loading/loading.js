
import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, Text } from 'react-native';
import Colors from '../../resources/colors'
import Dimens from '../../resources/dimens';

export default class Loading extends Component {  

  render() {

    let loadColor = this.props.loadColor ? this.props.loadColor : 'gray';

    if (this.props.hide) {
      return null;
    }

    let text = this.props.defaultText ? 'Carregando...' : '';
    if (this.props.text) {
      text = this.props.text;
    }

    let getText = () => {
      if (text.trim().length > 0) {
        return <Text style={{ fontSize: 12, textAlign: 'center', padding: Dimens.halfDefaultPadding }}>{text}</Text>
      }
      return null;
    }

    if (!this.props.noBackground) {
      return (
        <View style={styles.container}>
          <View style={[styles.container, styles.loadingPainel]} />
          <View style={{ elevation: 3, justifyContent: 'center', backgroundColor: Colors.defaultBackground, width: 100, height: 100 }}>
            <ActivityIndicator style={styles.loading} animating={this.props.visible} size="large" color={loadColor} />
            {getText()}
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ActivityIndicator style={[styles.spinner, { top: this.props.top }]} animating={this.props.visible} size="large" color={loadColor} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loadingPainel: {
    backgroundColor: Colors.blackTransparent,    
  },
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3
  },
  spinner: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
});
