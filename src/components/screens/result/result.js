import React, { PureComponent } from 'react';
import { Text, FlatList, View, StyleSheet, Platform } from 'react-native'
import AppComponent from '../../appComponent'
import LotteryCard from '../../lottery/lotteryCard'
import Card from '../../lottery/card'
import LinearGradient from 'react-native-linear-gradient'
import Loading from '../../loading/loading'
import ResultController from '../../../data/result/resultController'
import Colors from '../../../resources/colors'
import HTML from 'react-native-render-html'
import BoxButton from '../../button/boxButton'
import NoResult from '../../noResults'
import RNPickerSelect from 'react-native-picker-select'
import { Icon } from 'react-native-elements'

class ResultItem extends PureComponent {

    state = {
        isAsc: false,
    }

    orderDozens() {

        let item = this.props.item;
        if (!item.dezenas || item.dezenas.length === 0) {
            return [];
        }

        let dozens = item.dezenas.split('-');
        if (this.state.isAsc) {
            dozens.sort((a, b) => a - b);
        }
        return dozens;
    }

    setOrderButtonText() {
        if (this.state.isAsc) {
            return 'ORDEM DE SORTEIO'
        }
        return 'ORDEM CRESCENTE';
    }

    render() {

        let item = this.props.item;
        let buttonText = 'ORDEM DE SORTEIO';
        let dezenas = item.dezenas && item.dezenas.length > 0 ? item.dezenas.split('-') : [];

        let button = () => {
            return (item.tipo === '2' ? null : <BoxButton text={this.setOrderButtonText()} width={'100%'} color={Colors.linkBlue} action={
                () => { this.setState({ isAsc: !this.state.isAsc }); }
            } />)
        };
        let desc = '<b>' + item.sorteio + '</b></br>' + item.premio;

        return (
            <LinearGradient colors={['white', 'white', Colors.defaultBackground]} style={{ paddingHorizontal: 10, paddingBottom: 10, }}>
                <Card>
                    <View style={{ padding: 10, }}>
                        <HTML html={"<span style=' font-size: 18; text-align: center; paddingBottom: 20;'>" + desc + "</span>"} />
                        {
                            item.vencedores.map((obj, i) => {
                                return (
                                    <View key={i} style={{ justifyContent: 'center', }}>
                                        <Text style={{ padding: 5 }}>Certificado: {obj.numero}{"\n"}Nome:
                                            <Text style={{ fontWeight: 'bold' }}>{obj.nome}</Text>
                                            {"\n"}Bairro/Cidade: {obj.localidade}
                                        </Text>
                                        <View style={{ width: '100%', height: 1, backgroundColor: Colors.defaultBackground, }} />
                                    </View>
                                )
                            })
                        }
                        <View style={{ marginBottom: 10, paddingVertical: 5 }}>
                            <LotteryCard dozensSize={30} dozens={this.orderDozens()} />
                        </View>
                        {button()}
                    </View>
                </Card>
            </LinearGradient>
        )
    }
}

export default class Result extends AppComponent {

    state = {
        errorMsg: '',
        items: [{ label: '', value: '' }],
        lotteryDate: null
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadResultsDates();
    }

    loadResultsDates() {
        this.showLoading();
        ResultController.getResultDates(this.usertoken, this.params.info.empresa.id_tipo_loteria)
            .then((result) => {
                let dates = result.data.data;
                let items = this.createPickerItems(dates);
                this.setState({ dates: dates, items: items, lotteryDate: dates[0] }, () => {
                    this.loadResults(dates[0]);
                });
            }).catch((error) => {
                this.logMyErrors(error);
                this.hideLoading();
            });
    }

    loadResults(itemValue) {

        this.showLoading();
        let errorMsg = '';

        ResultController.getResults(this.usertoken, this.params.info.empresa.id_tipo_loteria, itemValue)
            .then((result) => {
                if (result.data.success === 'true') {
                    if (result.data.data.length === 0) {
                        errorMsg = Constants.APP_MSG_NO_RESULT;
                    }
                    this.setState({ results: result.data.data });
                } else {
                    alert('Não foi possível contactar o servidor.');
                    errorMsg = Constants.APP_MSG_NO_RESULT;
                }
                this.hideLoading(true, errorMsg);
            }).catch((error) => {
                this.logMyErrors(error);
                errorMsg = Constants.APP_MSG_NO_RESULT;
                this.hideLoading(true, errorMsg);
            });
    }

    hideLoading(hide, errorMsg) {
        this.setState({ hideLoading: hide, errorMsg, errorMsg });
    }

    createPickerItems(dates) {
        if (dates && dates.length > 0) {
            return dates.map((date, index) => {
                return (
                    { label: 'Sorteio - ' + date, value: date }
                )
            });
        }
        return [];
    }


    keyExtractor = (item, index) => index.toString();

    renderItem(mainObj, index) {
        let item = mainObj.item;
        return <ResultItem key={index} item={item} />
    }

    getPicker() {
        if (Platform.OS === 'ios') {
            return (
                <RNPickerSelect
                    placeholder={{}}
                    doneText='OK'
                    items={this.state.items}
                    onValueChange={(itemValue, index) => this.setState({ lotteryDate: itemValue })}
                    onOpen={() => {
                        this.oldLotteryDate = this.state.lotteryDate;
                    }}
                    onClose={() => {
                        if (this.oldLotteryDate !== this.state.lotteryDate) {
                            this.loadResults(this.state.lotteryDate)
                        }
                    }}
                    style={pickerSelectStyles}
                    value={this.state.lotteryDate}
                    Icon={() => <View style={{ height: 45, justifyContent: 'center', alignItems: 'center' }}><Icon name='menu-down' color={'white'} type='material-community' /></View>}
                />
            )
        } else {
            return (
                <RNPickerSelect
                placeholder={{}}
                items={this.state.items}
                onValueChange={(itemValue) => {
                    this.setState({ lotteryDate: itemValue });
                    this.loadResults(itemValue)
                }}
                style={pickerSelectStyles}
                value={this.state.items[this.state.lotteryDate]}
                Icon={() => <View style={{ height: 45, justifyContent: 'center', alignItems: 'center' }}><Icon name='menu-down' color={'white'} type='material-community' /></View>}
            />
            )
        }        
    }

    render() {        
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <FlatList
                    data={this.state.results}
                    ListEmptyComponent={<NoResult errorMsg={this.state.errorMsg} />}
                    ListHeaderComponent=
                    {
                        <View style={{ borderBottomColor: Colors.divisor, borderBottomWidth: 1, paddingHorizontal: 10, backgroundColor: this.mainColor + 'e6' }}>
                            {this.getPicker()}
                        </View>
                    }
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem} />
                <Loading loadColor={this.mainColor} defaultText hide={this.state.hideLoading} />
            </View>
        );
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingVertical: 12,
        paddingHorizontal: 10,
        width: '100%',
        color: 'white',
        paddingRight: 30 // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderRadius: 8,
        color: 'white',
        paddingRight: 30 // to ensure the text is never behind the icon
    }
});
