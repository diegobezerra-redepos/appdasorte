
import React, { Component } from 'react'
import { Image, Alert, View, StyleSheet, Text, Linking } from 'react-native'
import Loading from '../../loading/loading'
import Application from '../../../appConfig/application'
import Preferences from '../../../data/preferences/preferences'
import Colors from '../../../resources/colors'
import BoxButton from '../../button/boxButton'
import RNExitApp from 'react-native-exit-app'

export default class Splash extends Component {

  state = {
    hideLoading: false,
    errorText: '',
    config: null
  };

  constructor() {
    super();
    this.application = Application.instance;
  }

  componentDidMount() {
    this.loadConfigurationAndInitApp();
  }

  async loadConfigurationAndInitApp() {
    try {
      let config = await this.application.loadConfiguration();
      this.setState({ config: config }, async () => {
        await this.initConfig();
      });
    } catch (error) {
      Application.logError(error);
      this.setErrorState();
    }
  }

  async initConfig() {
    try {

      await this.application.initApp();
      let paramTime = this.application.parameters.info.visuzalizacao_termos.tempo;

      if (this.checkAppVersion()) {

        let isTermsAccepeted = await Preferences.isTermsAccepeted(paramTime);
        if (!isTermsAccepeted) {
          this.props.navigation.replace('Terms');
          return;
        }

        this.start();
      }

    } catch (error) {
      Application.logError(error);
      this.setErrorState();
    }
  }

  checkAppVersion() {
    if (this.application.parameters && this.application.parameters.newversion
      && this.application.parameters.marketplace && this.application.parameters.marketplace.url) {

      Alert.alert(
        'Alerta',
        this.application.parameters.aviso,
        [
          {
            text: 'Fechar Aplicativo',
            onPress: () => RNExitApp.exitApp(),
          },
          {
            text: 'Atualizar', onPress: () => {
              this.openUrl(this.application.parameters.marketplace.url);
            }
          },
        ],
        { cancelable: false },
      );

      return false;
    }
    return true;
  }

  openUrl(url) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (supported) {
          Linking.openURL(url);
          RNExitApp.exitApp();
        } else {
          console.log("Don't know how to open URI: " + this.props.url);
        }
      });
  }

  async start() {

    let beginDate = new Date();
    let params = this.application.parameters;
    let endDate = new Date();

    if (!params || params === 'error') {
      this.setErrorState();
      return;
    }

    let time = endDate - beginDate;
    if (time < this.MIN_SPLASH_TIME) {
      let timeout = this.MIN_SPLASH_TIME - time;
      setTimeout(() => {
        this.props.navigation.replace('Drawer');
      }, timeout);
    } else {
      this.props.navigation.replace('Drawer');
    }
  }

  setErrorState() {
    this.setState({
      hideLoading: true,
      errorText: 'Não foi possível contactar o servidor. Tente novamente mais tarde.'
    });
  }

  getRefreshButton() {
    if (this.state.errorText.length > 0) {
      return (
        <BoxButton style={{ marginTop: 30 }} color={Colors.linkBlue} width={180} text='Tentar Novamente' action={() => {
          this.setState({ hideLoading: false });
          this.initConfig();
        }} />
      )
    }

    return null;
  }

  render() {
    if (this.state.config) {
      return (
        <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
          <Image source={this.state.config.splash.splashImg} style={{ backgroundColor: 'white', height: 250, width: '100%' }} resizeMode='contain' />
          <Loading noBackground hide={this.state.hideLoading} top={200} />
          <Text style={[{ position: 'absolute', bottom: 0, left: 0, right: 0 }, styles.errorMsg]}>{this.state.errorText}</Text>
          {this.getRefreshButton()}
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
          <Loading noBackground hide={this.state.hideLoading} top={0} />
          <Text style={[styles.errorMsg]}>{this.state.errorText}</Text>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  errorMsg: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 10
  }
});
