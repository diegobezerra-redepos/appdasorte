import React from 'react'
import { Text, View, TextInput, StyleSheet, Alert, ScrollView } from 'react-native'
//import { CheckBox } from 'react-native-elements'
import Colors from '../../../resources/colors'
import Dimens from '../../../resources/dimens'
import { TextInputMask } from 'react-native-masked-text'
import Loading from '../../loading/loading'
import Validations, { InputEnum } from '../../../util/validations'
import UserController from '../../../data/user/userController'
import BoxButton from '../../button/boxButton'
import AppComponent from '../../appComponent'
import Application from '../../../appConfig/application'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class NewProfile extends AppComponent {

    static navigationOptions = ({ navigation }) => {
        let title = 'Novo Cadastro';
        return AppComponent.onTopNavigationOptions(navigation.getParam('barColor', 'red'), title, navigation);
    }

    userData = {
        id: '',
        nome: '',
        cpf: '',
        email: '',
        data_nascimento: '',
        cep: '',
        telefone: '',
        senha: '',
        senha_confirmacao: '',
        senha_antiga: '',
    }
    state = {
        userData: this.userData,
        hideLoading: true,
        buttonTitle: 'CADASTRAR',
    }

    componentDidMount() {
        super.componentDidMount();
    }

    validate() {
        let objs = Object.values(InputEnum);
        for (let index = 0; index < objs.length; index++) {
            const item = objs[index];
            let ref = this.refs[item.key];
            if (!ref) {
                continue;
            }
            let validation = Validations.validateByKey(item.key, ref);
            if (!validation) {
                return false;
            }
        }
        let userData = this.state.userData;
        if (!Validations.validatePasswordConfirmation(userData.senha_confirmacao, userData.senha, 'Senha')) {
            return false;
        }
        this.hideLoading();
        return true;
    }

    finilize() {
        if (this.validate()) {
            this.registerNewUser();
        }
    }

    exchangeCharacter(str, char, toChangeChar) {
        if (str.indexOf(char) !== -1) {
            str = str.replace(char, toChangeChar);
            str = this.exchangeCharacter(str, char, toChangeChar);
        }
        return str;
    }

    registerNewUser() {
        this.showLoading();
        this.state.userData.id_empresa = this.application.parameters.info.empresa.id_empresa;
        UserController.registerNewUser(this.state.userData)
            .then((result) => {
                this.setState({ hideLoading: true });
                if (result.data.success === 'true') {
                    this.showAlert('Sucesso!',this.exchangeCharacter(result.data.message, '<br>', '\n'));
                    this.props.navigation.dangerouslyGetParent().navigate('Home');
                    Application.logSignUp(true);
                } else {
                    let str = this.exchangeCharacter(result.data.message, '<br>', '\n');
                    this.showAlert('Alerta', str);
                    Application.logSignUp(false);
                }
            }).catch((error) => {
                this.showAlert('Erro', 'Houve um problema, tente novamente mais tarde.');
                this.setState({ hideLoading: true });
                this.logMyErrors(error);
                Application.logSignUp(false);
            });
    }

    getPasswordsInputs() {
        return (<View style={styles.passwordsView}>
            <TextInput
                ref={InputEnum.PASSWORD.key}
                {...inputAttrs}
                textContentType='password'
                maxLength={40}
                secureTextEntry
                style={styles.input}
                value={this.state.userData.senha}
                placeholder='Senha'
                onChangeText={(text) => { this.userData.senha = text.trim(); this.setState({ userData: this.userData }) }}
            />
            <TextInput
                ref={InputEnum.PASSWORD_CONFIRMATION.key}
                textContentType='password'
                {...inputAttrs}
                maxLength={40}
                secureTextEntry
                style={[{ borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }, styles.input]}
                value={this.state.userData.senha_confirmacao} placeholder='Confirmar Senha'
                onChangeText={(text) => { this.userData.senha_confirmacao = text.trim(); this.setState({ userData: this.userData }) }}
            />
        </View>)
    }

    render() {
        let getTopText = () => {
            return <View style={styles.topView}>
                <Text style={styles.infoText}>Preencha os dados para criar sua conta e participar dos sorteios do {this.config.appTitle}.</Text>
            </View>
        }

        let getCpfInput = () => {
            return <TextInputMask
                ref={InputEnum.CPF.key}
                {...inputAttrs}
                maxLength={14}
                type={'cpf'}
                keyboardType='numeric'
                style={styles.input}
                value={this.state.userData.cpf}
                onChangeText={(text) => { this.userData.cpf = text; this.setState({ userData: this.userData }) }}
                placeholder='Digite seu CPF' />
        }

        return (
            <View style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                <KeyboardAwareScrollView >
                    <View
                        style={styles.mainView}>
                        {getTopText()}
                        <View style={styles.inputView}>
                            <TextInput
                                placeholder='Nome completo'
                                {...inputAttrs}
                                ref={InputEnum.NAME.key}
                                value={this.state.userData.nome}
                                maxLength={100}
                                onChangeText={(text) => { this.userData.nome = text; this.setState({ userData: this.userData }) }}
                                style={[{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }, styles.input]} />
                            {getCpfInput()}
                            <TextInput
                                ref={InputEnum.EMAIL.key}
                                {...inputAttrs}
                                placeholder='E-mail'
                                value={this.state.userData.email}
                                onChangeText={(text) => { this.userData.email = text.trim(); this.setState({ userData: this.userData }) }}
                                maxLength={100}
                                style={styles.input} />
                            <TextInputMask
                                type='datetime'
                                keyboardType='numeric'
                                {...inputAttrs}
                                options={{ format: 'DD/MM/YYYY' }}
                                ref={InputEnum.BIRTH_DATE.key}
                                maxLength={10}
                                placeholder='Data nascimento'
                                onChangeText={(text) => { this.userData.data_nascimento = text; this.setState({ userData: this.userData }) }}
                                value={this.state.userData.data_nascimento}
                                style={styles.input} />
                            <TextInputMask
                                ref={InputEnum.ZIP_CODE.key}
                                keyboardType='numeric'
                                {...inputAttrs}
                                type='zip-code'
                                placeholder='CEP'
                                maxLength={9}
                                onChangeText={(text) => { this.userData.cep = text; this.setState({ userData: this.userData }) }}
                                value={this.state.userData.cep}
                                style={styles.input} />
                            <TextInputMask
                                ref={InputEnum.PHONE.key}
                                type='cel-phone'
                                {...inputAttrs}
                                keyboardType='numeric'
                                placeholder='Número de telefone (ddd + telefone)'
                                maxLength={15}
                                onChangeText={(text) => { this.userData.telefone = text; this.setState({ userData: this.userData }) }}
                                value={this.state.userData.telefone}
                                style={styles.input} />
                            {this.getPasswordsInputs()}
                            {/* <View style={styles.newsCheckBox}>
                                <CheckBox
                                    center={false}
                                    title='Desejo receber novidades por email'
                                    onPress={() => { this.userData.situacao = !this.userData.situacao; this.setState({ userData: this.userData }) }}
                                    checked={this.state.userData.situacao} />
                            </View> */}
                        </View>
                        <View style={styles.bottomView}>
                            <BoxButton text={this.state.buttonTitle} action={() => { this.finilize() }} width={defaultWitdh} />
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <Loading loadColor={this.mainColor} hide={this.state.hideLoading} />
            </View>
        );
    }
}

const defaultWitdh = '90%';
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        padding: Dimens.defaultPadding,
        //backgroundColor: 'gray',
    },
    inputView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        borderColor: Colors.blackTransparent,
        borderRadius: 5,
        //backgroundColor: 'pink'
    },
    topView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'yellow'
    },
    bottomView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        justifyContent: 'center',
    },
    input: {
        padding: Dimens.defaultPadding + Dimens.halfDefaultPadding,
        width: defaultWitdh,
        backgroundColor: 'white',
        color: Colors.fontColor,
        borderBottomColor: Colors.defaultBackground,
        borderBottomWidth: 1
    },
    logo: {
        alignSelf: 'center',
    },
    signInButton: {
        marginTop: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: Dimens.defaultRadius,
        width: defaultWitdh,
    },
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
    linkButton: {
        color: Colors.linkBlue,
        paddingTop: Dimens.halfDefaultPadding
    },
    infoText: {
        marginVertical: Dimens.defaultPadding,
        width: defaultWitdh,
        textAlign: 'center',
    },
    newsCheckBox: {
        flexDirection: 'row',
        width: defaultWitdh,
        paddingVertical: 10,
        alignItems: 'center'
    },
    passwordsView: {
        flex: 1,
        //backgroundColor: 'gray',
        width: '100%',
        alignItems: 'center'
    }
});
const inputAttrs = { placeholderTextColor: Colors.divisor };
