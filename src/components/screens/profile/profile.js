import React from 'react'
import { Text, TouchableOpacity, ScrollView, View, TextInput, StyleSheet, Image, Alert } from 'react-native'
import Colors from '../../../resources/colors'
import Dimens from '../../../resources/dimens'
import UserController from '../../../data/user/userController'
import Loading from '../../loading/loading'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'
import BoxButton from '../../button/boxButton'
import { TextInputMask } from 'react-native-masked-text'
import Validations from '../../../util/validations'
import AppComponent from '../../appComponent'
import Application from '../../../appConfig/application'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

export default class Profile extends AppComponent {

    static navigationOptions = ({ navigation }) => {
        return AppComponent.onTopNavigationOptions(navigation.getParam('barColor', 'red'), 'Acessar Conta', navigation);
    }

    state = {
        login: '',
        password: '',
        hideLoading: true,
        scrollViewHeight: null,
    }

    constructor() {
        super();
        this.state.scrollViewHeight = this.dimensions.height;
    }

    componentDidMount() {
        super.componentDidMount();
        this.callback = this.props.navigation.getParam('callback', null);
    }

    orientationDidChange(orientation) {
        let dimensions = this.getDimensions(orientation);
        this.setState({ scrollViewHeight: dimensions.height });
    }

    validateForm() {

        let login = this.refs['cpf'];

        if (!Validations.validateString(login.getRawValue())) {
            this.showAlert('Alerta', 'Preencha o login.');
            return false;
        } if (!Validations.validateString(this.state.password)) {
            this.showAlert('Alerta', 'Preencha a senha.');
            return false;
        }

        return true;
    }

    doLogin() {
        if (this.validateForm()) {
            this.setState({ hideLoading: false });

            let login = this.refs['cpf'].getRawValue();
            UserController.doLogin(login, this.state.password, this.params)
                .then((result) => {
                    this.setState({ hideLoading: true }, () => {
                        this.application.login(result.data.data, this.callback !== null);
                        Application.logLogin(true, result.data.data.usuario);
                        this.props.navigation.goBack();
                        this.callback && this.callback();
                    });
                }).catch((error) => {
                    this.showAlert('Alerta', error.message);
                    this.setState({ hideLoading: true });
                    this.logMyErrors(error);
                    Application.logLogin(false);
                });
        }
    }

    showAlert(title, msg) {
        Alert.alert(title, msg);
    }

    navigate(key, params) {
        this.props.navigation.push(key, params);
    }

    render() {
        return (
            <KeyboardAwareScrollView contentContainerStyle={{ height: this.state.scrollViewHeight, backgroundColor: Colors.defaultBackground }}>
                <View style={styles.mainView}>
                    <View style={styles.topView}>
                        <Image resizeMode='contain' style={{ height: 100, width: 100 }} source={this.config.appIcon} />
                    </View>
                    <View style={styles.inputView}>
                        <Text style={{ paddingVertical: 5, width: '85%', color: Colors.fontColor, textAlign: 'left' }}>Acessar Conta</Text>
                        <TextInputMask
                            ref={'cpf'}
                            {...inputAttrs}
                            maxLength={14}
                            type={'cpf'}
                            keyboardType='numeric'
                            style={[{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }, styles.input]}
                            value={this.state.login}
                            onChangeText={(text) => { this.setState({ login: text }) }}
                            placeholder='Digite seu CPF' />
                        <TextInput
                            {...inputAttrs}
                            style={[{ marginBottom: 10, borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }, styles.input]}
                            textContentType='password'
                            placeholder='Digite sua senha'
                            maxLength={40} secureTextEntry
                            onChangeText={(text) => { this.setState({ password: text }) }} />
                        <BoxButton text='ENTRAR' action={() => { this.doLogin() }} width='85%' />
                        <View style={styles.bottomView}>
                            <TouchableOpacityEx onPress={() => { this.navigate('ForgotPassword', { barColor: this.mainColor }) }}>
                                <Text style={styles.linkButton}>Esqueceu a senha?</Text>
                            </TouchableOpacityEx>
                            <TouchableOpacityEx onPress={() => { this.navigate('NewProfile', { barColor: this.mainColor, mode: 1 }) }}>
                                <Text style={styles.linkButton}>Crie sua conta</Text>
                            </TouchableOpacityEx>
                        </View>
                    </View>
                </View>
                <Loading loadColor={this.mainColor} hide={this.state.hideLoading} />
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: Colors.defaultBackground,
        padding: Dimens.defaultPadding
    },
    inputView: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        borderColor: Colors.blackTransparent,
        borderRadius: 5,
        //backgroundColor: 'yellow'
    },
    topView: {
        flex: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        //backgroundColor: 'blue'
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        //backgroundColor:'gray',
        width: '85%',
    },
    input: {
        padding: Dimens.defaultPadding + Dimens.halfDefaultPadding,
        width: '85%',
        color: Colors.fontColor,
        backgroundColor: 'white',
    },
    logo: {
        flex: 1,
    },
    signInButton: {
        marginTop: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: 5,
        width: '85%',
    },
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
    linkButton: {
        color: Colors.linkBlue,
        paddingTop: 5
    }
});
const inputAttrs = { placeholderTextColor: Colors.divisor };
