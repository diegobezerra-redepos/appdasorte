import React from 'react';
import { Text, View, TextInput, StyleSheet, Alert, ScrollView } from 'react-native';
//import { CheckBox } from 'react-native-elements'
import Colors from '../../../resources/colors'
import Dimens from '../../../resources/dimens';
import { TextInputMask } from 'react-native-masked-text'
import Loading from '../../loading/loading'
import Validations, { InputEnum } from '../../../util/validations'
import UserController from '../../../data/user/userController'
import BoxButton from '../../button/boxButton'
import AppComponent from '../../appComponent';
import Constants from '../../../services/constants'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class EditProfile extends AppComponent {

    static navigationOptions = ({ navigation }) => {
        let title = 'Meus Dados';
        return AppComponent.onTopNavigationOptions(navigation.getParam('barColor', 'red'), title, navigation);
    }

    userData = {
        id: '',
        nome: '',
        cpf: '',
        email: '',
        data_nascimento: '',
        cep: '',
        telefone: '',
        senha: '',
        senha_confirmacao: '',
        senha_antiga: '',
    }
    state = {
        userData: this.userData,
        userUpdateData: {},
        hideLoading: true,
        buttonTitle: 'ATUALIZAR',
    }

    componentDidMount() {
        super.componentDidMount();
        if (this.application.userData) {
            this.user = Object.assign({}, this.application.userData.usuario);
            this.setState({ userData: this.user });
        }
    }

    validate() {
        let userUpdateData = this.state.userUpdateData;
        let objs = Object.keys(userUpdateData);
        if (objs.length === 0) {
            this.showAlert('Alerta', 'Atualize seus dados antes de prosseguir.');
            return false;
        }

        //Validar senhas se alguma campo de senha for preenchido
        if (userUpdateData.senha_confirmacao || userUpdateData.senha || userUpdateData.senha_antiga) {
            if (!Validations.validateByKey(InputEnum.PASSWORD.key, this.refs[InputEnum.PASSWORD.key], true)) {
                return false;
            }
            if (!Validations.validateByKey(InputEnum.PASSWORD_CONFIRMATION.key, this.refs[InputEnum.PASSWORD_CONFIRMATION.key], true)) {
                return false;
            }
            if (!Validations.validateByKey(InputEnum.OLD_PASSWORD.key, this.refs[InputEnum.OLD_PASSWORD.key], true)) {
                return false;
            }
            if (!Validations.validatePasswordConfirmation(userUpdateData.senha_confirmacao, userUpdateData.senha, 'Nova Senha')) {
                return false;
            }
        }

        for (let index = 0; index < objs.length; index++) {
            const item = objs[index];
            let ref = this.refs[item];
            if (!ref) {
                continue;
            }
            let validation = Validations.validateByKey(item, ref, true);
            if (!validation) {
                return false;
            }
        }
        this.hideLoading();
        return true;
    }

    finilize() {
        if (this.validate()) {
            this.updateUser();
        }
    }

    exchangeCharacter(str, char, toChangeChar) {
        if (str.indexOf(char) !== -1) {
            str = str.replace(char, toChangeChar);
            str = this.exchangeCharacter(str, char, toChangeChar);
        }
        return str;
    }

    setDefaultValue(inputType, data) {
        let userData = this.state.userData;
        if (!data[inputType.key]) {
            data[inputType.key] = userData[inputType.key];
        }
    }

    updateCachedUserData(data) {
        let userData = Object.assign({}, this.application.userData.usuario, data);
        delete userData[InputEnum.PASSWORD.key];
        delete userData[InputEnum.PASSWORD_CONFIRMATION.key];
        delete userData[InputEnum.OLD_PASSWORD.key];
        return userData;
    }

    updateUser() {
        //alert(JSON.stringify(this.state.userUpdateData));
        //return;
        this.showLoading();

        let data = Object.assign({}, this.state.userUpdateData);

        data.id = this.application.userData.usuario.id;
        data.id_endereco = this.application.userData.usuario.id_endereco;
        data.id_telefone = this.application.userData.usuario.id_telefone;
        this.setDefaultValue(InputEnum.NAME, data);
        this.setDefaultValue(InputEnum.EMAIL, data);
        this.setDefaultValue(InputEnum.BIRTH_DATE, data);
        this.setDefaultValue(InputEnum.ZIP_CODE, data);
        this.setDefaultValue(InputEnum.PHONE, data);

        this.state.userData.senha = '';
        this.state.userData.senha_antiga = '';
        this.state.userData.senha_confirmacao = '';

        UserController.updateUser(this.application.userData.token, data, this.params.info.empresa.id_empresa)
            .then((result) => {
                if (result.data) {
                    let message = result.data.message;
                    if (result.data.success === 'true') {
                        let updatedData = this.updateCachedUserData(data);
                        this.application.onUpdateUserData(updatedData);
                        this.props.navigation.dangerouslyGetParent().navigate('Home');
                    }
                    alert(this.exchangeCharacter(message, '<br>', '\n'));
                } else {
                    alert(Constants.APP_MSG_UNAVAILABLE);
                }
                this.hideLoading();
            }).catch((error) => {
                this.logMyErrors(error);
                alert(Constants.APP_MSG_UNAVAILABLE);
                this.hideLoading();
            });
    }

    _onChangeText(propertyName, newValue) {

        let userUpdateData = this.state.userUpdateData;
        let userData = this.state.userData;
        userData[propertyName] = newValue;

        if (!Validations.validateString(newValue)
            && (propertyName === InputEnum.PASSWORD.key
                || propertyName === InputEnum.PASSWORD_CONFIRMATION.key
                || propertyName === InputEnum.OLD_PASSWORD.key)) {

            delete userUpdateData[propertyName];

        } else {
            userUpdateData[propertyName] = newValue;
        }

        this.setState({ userData: userData, userUpdateData: userUpdateData });
    }

    getPasswordsInputs() {
        return (
            <View style={styles.passwordsView}>
                <TextInput style={styles.input}
                    {...inputAttrs}
                    ref={InputEnum.OLD_PASSWORD.key}
                    value={this.state.userData.senha_antiga}
                    placeholder='Senha Antiga'
                    textContentType='password'
                    maxLength={40}
                    secureTextEntry
                    onChangeText={(text) => { this._onChangeText(InputEnum.OLD_PASSWORD.key, text) }}
                />
                <TextInput
                    ref={InputEnum.PASSWORD.key}
                    {...inputAttrs}
                    textContentType='password'
                    maxLength={40}
                    secureTextEntry
                    style={styles.input}
                    value={this.state.userData.senha}
                    placeholder='Nova Senha'
                    onChangeText={(text) => { this._onChangeText(InputEnum.PASSWORD.key, text) }}
                />
                <TextInput
                    ref={InputEnum.PASSWORD_CONFIRMATION.key}
                    {...inputAttrs}
                    textContentType='password'
                    maxLength={40}
                    secureTextEntry
                    style={[{ borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }, styles.input]}
                    value={this.state.userData.senha_confirmacao} placeholder='Confirmar Senha'
                    onChangeText={(text) => { this._onChangeText(InputEnum.PASSWORD_CONFIRMATION.key, text) }}
                />
            </View>
        )
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                <KeyboardAwareScrollView >
                    <View style={styles.mainView}>
                        <View style={styles.inputView}>
                            <TextInput
                                placeholder='Nome completo'
                                {...inputAttrs}
                                ref={InputEnum.NAME.key}
                                value={this.state.userData.nome}
                                onChangeText={(text) => { this._onChangeText(InputEnum.NAME.key, text) }}
                                maxLength={100}
                                style={[{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }, styles.input]} />
                            <TextInput
                                ref={InputEnum.EMAIL.key}
                                {...inputAttrs}
                                placeholder='E-mail'
                                maxLength={100}
                                value={this.state.userData.email}
                                onChangeText={(text) => { this._onChangeText(InputEnum.EMAIL.key, text.trim()) }}
                                style={styles.input} />
                            <TextInputMask
                                type='datetime'
                                {...inputAttrs}
                                keyboardType='numeric'
                                options={{ format: 'DD/MM/YYYY' }}
                                ref={InputEnum.BIRTH_DATE.key}
                                maxLength={10}
                                placeholder='Data nascimento'
                                onChangeText={(text) => { this._onChangeText(InputEnum.BIRTH_DATE.key, text) }}
                                value={this.state.userData.data_nascimento}
                                style={styles.input} />
                            <TextInputMask
                                ref={InputEnum.ZIP_CODE.key}
                                {...inputAttrs}
                                keyboardType='numeric'
                                type='zip-code'
                                placeholder='CEP'
                                maxLength={9}
                                onChangeText={(text) => { this._onChangeText(InputEnum.ZIP_CODE.key, text) }}
                                value={this.state.userData.cep}
                                style={styles.input} />
                            <TextInputMask
                                ref={InputEnum.PHONE.key}
                                {...inputAttrs}
                                type='cel-phone'
                                keyboardType='numeric'
                                placeholder='Número de telefone (ddd + telefone)'
                                maxLength={15}
                                onChangeText={(text) => { this._onChangeText(InputEnum.PHONE.key, text) }}
                                value={this.state.userData.telefone}
                                style={styles.input} />
                            {this.getPasswordsInputs()}
                            {/* <View style={styles.newsCheckBox}>
                                <CheckBox
                                    center={false}
                                    title='Desejo receber novidades por email'
                                    onPress={() => { this.userData.situacao = !this.userData.situacao; this.setState({ userData: this.userData }) }}
                                    checked={this.state.userData.situacao} />                                
                            </View> */}
                        </View>
                        <View style={styles.bottomView}>
                            <BoxButton text={this.state.buttonTitle} action={() => { this.finilize() }} width={defaultWitdh} />
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <Loading loadColor={this.mainColor} hide={this.state.hideLoading} />
            </View>
        );
    }
}

const defaultWitdh = '90%';
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        padding: Dimens.defaultPadding,
        //backgroundColor: 'gray',
    },
    inputView: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth: 0,
        borderColor: Colors.blackTransparent,
        borderRadius: 5,
        //backgroundColor: 'pink'
    },
    topView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'yellow'
    },
    bottomView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        justifyContent: 'center',
    },
    input: {
        padding: Dimens.defaultPadding + Dimens.halfDefaultPadding,
        width: defaultWitdh,
        backgroundColor: 'white',
        color: Colors.fontColor,
        borderBottomColor: Colors.defaultBackground,
        borderBottomWidth: 1
    },
    logo: {
        alignSelf: 'center',
    },
    signInButton: {
        marginTop: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        backgroundColor: Colors.positiveGreen,
        borderRadius: Dimens.defaultRadius,
        width: defaultWitdh,
    },
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
    linkButton: {
        color: Colors.linkBlue,
        paddingTop: Dimens.halfDefaultPadding
    },
    infoText: {
        marginVertical: Dimens.defaultPadding,
        width: defaultWitdh,
        textAlign: 'center',
    },
    newsCheckBox: {
        flexDirection: 'row',
        width: defaultWitdh,
        paddingVertical: 10,
        alignItems: 'center'
    },
    passwordsView: {
        flex: 1,
        //backgroundColor: 'gray',
        width: '100%',
        alignItems: 'center'
    }
});
const inputAttrs = { placeholderTextColor: Colors.divisor };
