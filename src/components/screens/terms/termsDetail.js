
import React from 'react'
import { WebView, ActivityIndicator, View } from 'react-native'
import AppComponent from '../../appComponent'
import NoResults from '../../noResults'
import Pdf from 'react-native-pdf';

let url = '';
let title = '';
export default class TermsDetail extends AppComponent {

    state = {
        loading: true
    }

    static navigationOptions = ({ navigation }) => {
        url = navigation.getParam('url', '');
        title = navigation.getParam('title', '');
        const mainColor = navigation.getParam('mainColor', 'red');
        return AppComponent.onTopNavigationOptions(mainColor, title, navigation);
    }

    componentDidMount() {
        super.componentDidMount();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Pdf
                    source={{ uri: url }}
                    style={{ flex: 1, width: '100%' }} />
            </View>
        )
    }
}