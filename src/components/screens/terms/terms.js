
import React from 'react';
import { ScrollView, TouchableOpacity, Text, SafeAreaView, Platform, View, Image, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import Colors from '../../../resources/colors'
import BoxButton from '../../button/boxButton';
import HTML from 'react-native-render-html'
import AppComponent from '../../appComponent'
import Loading from '../../loading/loading'

export default class Terms extends AppComponent {

    state = {
        isTermsAccepeted: false,
        loading: true
    }

    componentDidMount() {
        super.componentDidMount();
        this.getTermsAccepted();
    }

    async getTermsAccepted() {
        try {
            let isTermsAccepeted = await this.Preferences.isTermsAccepeted();
            this.setState({ loading: false, isTermsAccepeted: isTermsAccepeted });
        } catch (error) {
            this.logMyErrors(error);
        }
    }

    async agreedAction() {
        try {
            await this.Preferences.setTermsAccepted(true);
            this.props.navigation.replace('Drawer');
        } catch (error) {
            this.logMyErrors(error);
        }
    }

    openTermsDetails(url, title) {
        this.props.navigation.dangerouslyGetParent().navigate(
            'TermsDetail', {
                url: url,
                title: title,
                mainColor: this.mainColor
            });
    }

    render() {

        const appleDisclaimer = Platform.OS === 'ios' ?
            <Text style={styles.appleDisclaimer}>*A Apple não participa das operações de vendas e premiações.</Text> : null

        let term = this.params.info.termos;
        let TermItem = (props) => {
            return (
                <TouchableOpacity onPress={() => { this.openTermsDetails(props.link, props.titulo) }}>
                    <View style={[props.style, styles.termView]}>
                        <Icon name='book-open' color={Colors.fontColor} type='simple-line-icon' />
                        <Text style={styles.termText}>{props.titulo}</Text>
                    </View>
                </TouchableOpacity>
            )
        }

        if (this.state.loading) {
            return (
                <Loading color={this.mainColor} hide={false} />
            )
        }

        if (this.state.isTermsAccepeted) {
            return (
                <SafeAreaView style={styles.main}>
                    <Image source={this.config.splash.splashImg} style={{ width: '60%', height: 150 }} resizeMode='contain' />
                    <View style={{ marginVertical: 10, width: '100%' }}>
                        <TermItem link={term.privacidade.link} titulo={term.privacidade.titulo} />
                        <TermItem link={term.termo.link} titulo={term.termo.titulo} />
                        <TermItem style={{ borderBottomWidth: 1, borderBottomColor: Colors.divisor }} link={term.regulamento.link} titulo={term.regulamento.titulo} />
                    </View>
                    {appleDisclaimer}
                </SafeAreaView>
            )
        } else {
            return (
                <SafeAreaView style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                    <ScrollView contentContainerStyle={{ paddingBottom: 10 }} >
                        <View style={styles.main}>
                            <Image source={this.config.splash.splashImg} style={{ width: '60%', height: 150 }} resizeMode='contain' />
                            <HTML html={"<span style='text-align:justify; padding: 16px; font-size: 13;'>" + this.params.info.visuzalizacao_termos.texto + "</span>"} />
                            <View style={{ marginVertical: 10, width: '100%' }}>
                                <TermItem link={term.privacidade.link} titulo={term.privacidade.titulo} />
                                <TermItem link={term.termo.link} titulo={term.termo.titulo} />
                                <TermItem style={{ borderBottomWidth: 1, borderBottomColor: Colors.divisor }} link={term.regulamento.link} titulo={term.regulamento.titulo} />
                            </View>
                            <BoxButton width={'95%'} text='CONCORDO' color={Colors.linkBlue} action={() => { this.agreedAction() }} />
                            {appleDisclaimer}
                        </View>
                    </ScrollView>
                </SafeAreaView>
            );
        }
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: Colors.defaultBackground
    },
    termView: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: Colors.divisor,
        padding: 10,
        backgroundColor: 'white'
    },
    termText: {
        marginHorizontal: 10,
    },
    appleDisclaimer: {
        fontSize: 13,
        width: '100%',
        padding: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    }
});
