
import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import BadgeNotificationsController from '../../../data/badgeNotifications/badgeNotificationsController'
import Badge from './badge'

export default class HeaderLeft extends Component {

    state = {
        badge: null
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    componentDidMount() {
        this.unmounted = false;
        // BadgeNotificationsController.addDrawerIconState(this.setState.bind(this));
        // this.loadBadge();
    }

    async loadBadge() {
        let iconBadge = await BadgeNotificationsController.retrieveBadgeNotificationsForDrawerIcon();
        if (!this.unmounted) {
            this.setState({ badge: iconBadge });
        }
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    if (this.props.navigation) {
                        this.props.navigation.toggleDrawer();
                    }
                }}>
                    <View style={NavStyles.dehazeIcon}>
                        <Icon name='dehaze' color={this.props.iconColor} />
                        <Badge badgeValue={this.state.badge} style={NavStyles.badge} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const NavStyles = StyleSheet.create({
    headerStyle: {
        backgroundColor: 'gray',
    },
    headerTitleStyle: {
        color: 'white',
    },
    dehazeIcon: {
        margin: 8,
    },
    badge: {
        right: -5,
        top: -5,
        width: 15,
        height: 15,
        fontSize: 10,
    }
});