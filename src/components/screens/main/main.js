import React, { Component } from 'react'
import { Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import Splash from '../splash/splash'
import CustomDrawerContentComponent from './drawerContent'
import { menuItems } from './menuItems'
import Profile from '../profile/profile'
import NewProfile from '../profile/newProfile'
import EditProfile from '../profile/editProfile'
import Terms from '../terms/terms'
import ForgotPassword from '../profile/forgotPassword'
import { fromBottom } from '../../../util/transitions'
import TermsDetail from '../terms/termsDetail';
import Application from '../../../appConfig/application';

const persistenceKey = "persistenceKey"
const persistNavigationState = async (navState) => {
    try {
        await AsyncStorage.setItem(persistenceKey, JSON.stringify(navState))
    } catch (err) {
        // handle the error according to your needs
    }
}
const loadNavigationState = async () => {
    const jsonString = await AsyncStorage.getItem(persistenceKey)
    return JSON.parse(jsonString)
}

export default class Main extends Component {

    render() {
        const drawerConfig = Platform.select({
            ios: () => { return { contentComponent: CustomDrawerContentComponent, drawerWidth: 285, overlayColor: "rgba(0, 0, 0, 0.7)" } },
            android: () => { return { contentComponent: CustomDrawerContentComponent, overlayColor: "rgba(0, 0, 0, 0.7)" } }
        })();
        const Drawer = createDrawerNavigator(menuItems(Application.instance), drawerConfig);
        const StackNav = createStackNavigator({
            Splash: {
                screen: Splash,
            },
            Drawer: {
                screen: Drawer
            },
            Profile: {
                screen: Profile,
            },
            NewProfile: {
                screen: NewProfile,
            },
            EditProfile: {
                screen: EditProfile
            },
            ForgotPassword: {
                screen: ForgotPassword,
            },
            Terms: {
                screen: Terms
            },
            TermsDetail: {
                screen: TermsDetail
            }
        }, {
            transitionConfig: () => fromBottom(), defaultNavigationOptions: {
                header: null
            }
        });
        const Container = createAppContainer(StackNav)

        return <Container
            persistNavigationState={persistNavigationState}
            loadNavigationState={loadNavigationState} />
    }
}
