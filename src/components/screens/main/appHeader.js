
import React from 'react';
import HeaderLeft from './headerLeft';
import CheckoutHeader from './checkoutHeader'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { Header, BackHandler } from 'react-navigation-stack';
import Dimens from '../../../resources/dimens'

export default class AppHeader {

    static loadCssParameters = (navigation, appParams, title, checkoutHeaderAction) => {
        let params = appParams;
        let opt = {
            headerLeft: <HeaderLeft iconColor='white' navigation={navigation} />,
            headerRight: <CheckoutHeader navigation={navigation} action={checkoutHeaderAction} />,
            headerStyle: { backgroundColor: params.css.strCorBarra },
            headerTitleStyle: styles.headerTitleStyle,
        }
        if (title) {
            Object.assign(opt, { title: title });
        }

        return opt;
    }

    static onTopNavigationOptions = (barColor, title, navigation) => {

        return {
            title: title,
            header: (headerProps) => <Header {...headerProps} />,
            headerLeft: null,
            headerRight: <TouchableOpacity style={{ padding: Dimens.defaultPadding }} onPress={() => { navigation.goBack() }}>
                <Text style={{
                    color: 'white',
                    alignSelf: 'center'
                }} >Fechar</Text>
            </TouchableOpacity>,
            headerStyle: { backgroundColor: barColor },
            headerTitleStyle: { color: 'white' },
        }
    }

    static profileHeaderRight = (navigation, title) => {
        return {
            title: title,
            headerRight:
                <TouchableOpacity style={{ padding: Dimens.defaultPadding }} onPress={() => { navigation.goBack() }}>
                    <Text style={styles.textButton} >Fechar</Text>
                </TouchableOpacity>,
            headerLeft: null
        }
    }

    static checkoutScreenHeader = (navigation, appParams, title, leftButton) => {
        let obj = Object.assign({}, AppHeader.loadCssParameters(navigation, appParams, title), {
            title: title,
            headerLeft: BackHandler,
            headerRight: null,
            headerTintColor: 'white'
        });
        if (leftButton) {
            obj.headerLeft = leftButton;
        }

        return obj;
    }
}

const styles = StyleSheet.create({
    textButton: {
        color: 'white',
        alignSelf: 'center'
    },
    headerStyle: {
        backgroundColor: 'gray',
    },
    headerTitleStyle: {
        color: 'white',
    },
    dehazeIcon: {
        margin: 8,
    },
});