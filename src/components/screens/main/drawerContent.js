
import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, View, SafeAreaView, Text, TouchableOpacity, TouchableNativeFeedback, Platform, Linking, Alert } from 'react-native'
import Colors from '../../../resources/colors'
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import Dimens from '../../../resources/dimens'
import { Icon } from 'react-native-elements'
import Application from '../../../appConfig/application'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'
import BadgeNotificationsController from '../../../data/badgeNotifications/badgeNotificationsController'
import { DrawerLabel } from './menuItems'
import DeviceInfo from 'react-native-device-info'

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);
const TouchableNativeFeedbackEx = withPreventDoubleClick(TouchableNativeFeedback);

export default class CustomDrawerContentComponent extends Component {

    state = {
        userData: null,
        showUserMenu: false,
        userMenuBackColor: 'gray'
    }

    constructor() {
        super();
        this.application = Application.instance;
    }

    componentDidMount() {
        this.application.setDrawerNavigation(this.props.navigation.dangerouslyGetParent());
        this.state.userMenuBackColor = this.application.parameters.css.strCorBarra;
        this.state.userData = this.application.userData;
        this.application.onLogin = this.onLogin.bind(this);
        this.application.onLogout = this.logout.bind(this);
    }

    filterDrawerItems(isUserLogged) {

        const { items } = this.props;

        if (isUserLogged) {
            this.filteredItems = items;
        } else {
            const filter = ['MyCertificatates', 'MarkCertificatates', 'PendingCertificatates', 'CanceledCertificatates', 'Configuration'];
            this.filteredItems = items.filter(item => !filter.includes(item.key));
        }
    }

    onLogin = (userData, dontNavigateHome) => {
        this.setState({ userData: userData });
        if (!dontNavigateHome) {
            this.props.navigation.navigate('Home');
        }
    }

    logout() {
        Alert.alert(
            'Alerta',
            'Sair da sua conta?',
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                {
                    text: 'Sair',
                    onPress: () => {
                        this.application.removeUserData();
                        this.props.navigation.navigate('Home');
                        this.setState({ userData: null, showUserMenu: false });
                    }
                },
            ],
            { cancelable: true },
        );
    }

    appInfo = () => {
        return null;
        // if (false) {
        // if (!this.application.isProd || (this.application.isProd && __DEV__)) {
        //     let text = 'baseURL: ' + this.application.configurations.baseURL + '\nflavor: ' + this.application.configurations.base + '\nsellURL: ' + this.application.finalizeSellUrl;
        //     return <Text style={{ padding: Dimens.defaultPadding }}>{text}</Text>
        // }
    }

    openUrl(url) {
        Linking.canOpenURL(url)
            .then(supported => {
                if (supported) {
                    Linking.openURL(url);
                } else {
                    console.log("Don't know how to open URI: " + this.props.url);
                }
            });
    }

    getUserContent() {
        let mainColor = this.state.userMenuBackColor;
        if (this.application.userData) {
            return (
                <View>
                    <CustomDrawerItem text='Meus Dados' icon='user' action={() => {
                        this.props.navigation.navigate('EditProfile', { barColor: mainColor })
                    }} />
                    <CustomDrawerItem text='Sair' icon='logout' action={() => {
                        this.logout();
                        this.props.navigation.toggleDrawer();
                    }} />
                </View>
            )
        } else {
            return (
                <View>
                    <CustomDrawerItem text='Acessar Conta' icon='login' action={() => {
                        this.props.navigation.navigate('Profile', { barColor: mainColor });
                    }} />
                </View>
            )
        }
    }

    render() {
        this.filterDrawerItems(this.state.userData != null);

        if (!this.state.showUserMenu) {
            return (
                <View style={styles.container}>
                    <SafeAreaView style={{ backgroundColor: Colors.defaultBackground }}>
                        <View style={styles.userView}>
                            <Image style={styles.logo} resizeMode='contain' source={this.application.configurations.splash.splashImg} />
                            <UserContent mainState={this.state} nav={this.props.navigation} action={() => { this.setState({ showUserMenu: true }) }} />
                            <View style={styles.divisor} />
                        </View>
                    </SafeAreaView>
                    <ScrollView
                    contentContainerStyle={{ paddingBottom: 20 }}
                    style={styles.itemsContainer} >
                        <DrawerNavigatorItems {...this.props} items={this.filteredItems}
                            onItemPress={
                                (obj, focused) => {
                                    setTimeout(() => {
                                        this.props.navigation.navigate(obj.route.key);
                                    }, 10);
                                    BadgeNotificationsController.removeBadgeNotificationData(obj.route.key);
                                }
                            } />
                        {this.getUserContent()}
                        {this.appInfo()}
                    </ScrollView>
                </View >
            );
        } else {
            return <View style={styles.container}>
                <SafeAreaView style={{ backgroundColor: Colors.defaultBackground }}>
                    <View style={styles.userView}>
                        <Image style={styles.logo} resizeMode='contain' source={this.application.configurations.splash.splashImg} />
                        <UserContent mainState={this.state} action={() => { this.setState({ showUserMenu: false }) }} />
                        <View style={styles.divisor} />
                    </View>
                </SafeAreaView>
                <View>
                    <CustomDrawerItem text='Meus Dados' icon='user' action={() => { this.props.navigation.navigate('NewProfile', { barColor: this.state.userMenuBackColor }) }} />
                    <CustomDrawerItem text='Sair' icon='logout' action={() => { this.logout() }} />
                </View>
                {this.appInfo()}
            </View >
        }
    }
}

class UserContent extends Component {

    componentDidMount() {
        console.log('aqui ' + DeviceInfo.getVersion());
    }

    render() {

        let appInfo = () => <Text style={{ fontSize: 10, alignSelf: 'flex-start', paddingLeft: 3 }}>{DeviceInfo.getVersion()}</Text>;

        if (this.props.mainState.userData) {
            let userName = this.props.mainState.userData && !this.props.mainState.showUserMenu ? this.props.mainState.userData.usuario.nome : '';
            return (
                <View style={{
                    flexDirection: 'column',
                    width: '100%',
                    height: 50,
                }}>
                    {appInfo()}
                    <View style={[styles.userContainer, { backgroundColor: this.props.mainState.userMenuBackColor, justifyContent: 'flex-start' }]}>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={styles.userName}>{userName}</Text>
                    </View>
                </View>
            )
        } else {
            return (appInfo())
        }

        // let Button = (props) => (
        //     <TouchableOpacityEx
        //         style={[styles.userContainer, { backgroundColor: this.state.userMenuBackColor, justifyContent: props.justifyContentVal ? props.justifyContentVal : 'flex-start' }]}
        //         onPress={() => { if (props.action) props.action(); }}>
        //         {props.children}
        //     </TouchableOpacityEx>)

        // if (this.props.mainState.userData) {
        //     if (!this.props.mainState.showUserMenu) {
        //         return (
        //             <Button justifyContentVal='space-between' action={() => {
        //                 //this.props.action()
        //             }}>
        //                 <Text style={styles.userName}>{this.props.mainState.userData.usuario.nome}</Text>
        //                 {/* <Icon style={styles.logoutIcon} name='arrow-right' color='white' type='simple-line-icon' /> */}
        //             </Button>)
        //     } else {
        //         return (
        //             <Button action={() => { this.props.action() }}>
        //                 <Icon style={styles.logoutIcon} name='arrow-left' color='white' type='simple-line-icon' />
        //                 <Text style={styles.userName}>Voltar</Text>
        //             </Button>)
        //     }
        // } else {
        //     return (
        //         <Button justifyContentVal='space-between' action={() => { this.props.nav.navigate('Profile', { barColor: this.state.userMenuBackColor }); }}>
        //             <Text style={styles.userName}>Acessar conta</Text>
        //             <Icon style={styles.logoutIcon} name='login' color='white' type='simple-line-icon' />
        //         </Button>)
        // }
    }
}

class CustomDrawerItem extends Component {

    render() {
        if (Platform.OS === 'ios') {
            return (
                <TouchableOpacityEx onPress={() => {
                    this.props.action();
                }}>
                    <View style={{ paddingHorizontal: 15, alignItems: 'center', flexDirection: 'row' }}>
                        <Icon name={this.props.icon} color='gray' type='simple-line-icon' />
                        <View style={{ flex: 1, paddingLeft: 13 }}>
                            <DrawerLabel tintColor='' itemKey='' draweTitle={this.props.text} />
                        </View>
                    </View>
                </TouchableOpacityEx>
            )
        }
        return (
            <TouchableNativeFeedback onPress={() => {
                this.props.action();
            }}>
                <View style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name={this.props.icon} color='gray' type='simple-line-icon' />
                    <View style={{ flex: 1, paddingLeft: 13 }}>
                        <DrawerLabel tintColor='' itemKey='' draweTitle={this.props.text} />
                    </View>
                </View>
            </TouchableNativeFeedback>
        )
    }
}

const styles = StyleSheet.create({
    userContainer: {
        flexDirection: 'row',
        width: '100%',
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Dimens.defaultPadding
    },
    userName: {
        fontSize: 18,
        color: 'white'
    },
    logoutIcon: {
        alignSelf: 'flex-end'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    userView: {
        backgroundColor: Colors.defaultBackground,
        width: '100%',
        height: 163,
        alignItems: 'center',
        flexDirection: 'column',
    },
    logo: {
        flex: 3,
        width: '60%',
    },
    divisor: {
        width: '100%',
        height: 1,
        backgroundColor: Colors.divisor,
    },
    itemsContainer: {
        flex: 1,
    },
    item: {
        paddingStart: Dimens.defaultPadding,
        paddingBottom: Dimens.defaultPadding,
        paddingTop: Dimens.defaultPadding,
        paddingEnd: Dimens.defaultPadding,
    },
    itemText: {
        marginStart: Dimens.iconPadding,
        fontWeight: 'bold',
        color: Colors.fontColor,
    },
    itemView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
});