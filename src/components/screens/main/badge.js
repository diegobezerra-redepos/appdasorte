import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Badge extends Component {

    render() {
        if (!this.props.noBadgeValue) {
            if (!this.props.badgeValue) {
                return null;
            }
        }
        return (
            <View style={[styles.badge, this.props.style]}>
                <Text style={{ color: 'white', fontSize: 11, fontWeight: 'bold' }}>
                    {this.props.badgeValue}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    badge: {
        borderRadius: 30,
        backgroundColor: 'red',
        position: 'absolute',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white',
        top: 18,
        right: 70,
        bottom: 10,
        width: 20,
        height: 20,
    },
});