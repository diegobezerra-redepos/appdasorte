import React from 'react'
import { ScrollView, TouchableOpacity, Text, StyleSheet, View } from 'react-native'
import AppComponent from '../../appComponent'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import MapController from '../../../data/map/mapController'
import Colors from '../../../resources/colors'
import Loading from '../../loading/loading'
import Dimens from '../../../resources/dimens'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);
const DISTANCE = 6.21371;
const deltaValue = 0.01;

export default class CertificationMap extends AppComponent {

    state = {
        data: null,
        statusBarHeight: 0,
        hideLoading: false,
        allowDragging: true,
        height: 0,
        visible: true,
    }

    constructor() {
        super();
        this.isPanelOpen = false;
        this.visible = true;
    }

    componentDidMount() {
        super.componentDidMount();
        this.getDimensionsByOrientation((dimensions) => {
            this.height = dimensions.height;
            this.initCoordinates();
        });
    }

    componentWillUnmount() {
    }

    initCoordinates() {
        this.loadMapData();
    }

    async loadMapData() {

        if (!this.params.info || !this.params.info.empresa) {
            return;
        }

        this.coords = {};
        let coordsAux = await this.application.getCoordinates();
        if (coordsAux && coordsAux.latitude != 0) {
            this.coords.latitude = coordsAux.latitude;
            this.coords.longitude = coordsAux.longitude;
            this.coords.latitudeDelta = deltaValue;
            this.coords.longitudeDelta = deltaValue;
            let companyId = this.params.info.empresa.id_empresa;

            this.getSellPointsData(companyId, this.coords.latitude, this.coords.longitude);

        } else {
            //Brasil
            this.coords.latitude = -9.492655;
            this.coords.longitude = -55.038038;
            this.coords.latitudeDelta = 10;
            this.coords.longitudeDelta = 10;

            this.setState({
                hideLoading: true,
                statusBarHeight: 0.5,
                height: this.height
            });
        }
    }

    getSellPointsData(companyId, latitude, longitude) {
        let geoCodeKey = this.application.configurations.geocodingKey;
        MapController.getSellPointsData(companyId, latitude, longitude, DISTANCE, geoCodeKey)
            .then((result) => {
                if (result.codigo_execucao === 0) {
                    this.setState({
                        data: result.dados,
                        hideLoading: true,
                        statusBarHeight: 0.5,
                        height: this.height
                    });
                }
                this.hideLoading();
            }).catch((error) => {
                this.logMyErrors(error);
                this.hideLoading();
            });
    }

    renderMarkers() {

        if (!this.state.data) {
            return;
        }

        let arr = new Array();
        if (this.state.data) {
            this.state.data.map((marker, index) => {

                let address = this.createAddressFromData(marker);
                let info = null;
                if (address) {
                    info = <MapView.Callout tooltip style={styles.customView}>
                            <View style={{ backgroundColor: 'white', padding: 10, marginBottom: 5, borderRadius: 5 }}>
                                <Text style={{ paddingTop: 5, color: Colors.fontColor, fontSize: 20, }}>
                                    {address.firstLine}
                                </Text>
                                <Text style={{ paddingBottom: 5, color: Colors.fontColor, fontSize: 18 }}>
                                    {address.secondLine}
                                </Text>
                            </View>
                        </MapView.Callout>
                }

                arr.push(
                    <Marker
                        key={index}
                        onPress={() => this._map.animateCamera({
                            latitude: parseFloat(marker.GPS_LATITUDE),
                            longitude: parseFloat(marker.GPS_LONGITUDE)
                        }, 1000)}
                        coordinate={{ latitude: parseFloat(marker.GPS_LATITUDE), longitude: parseFloat(marker.GPS_LONGITUDE), }}>
                        {info}
                    </Marker>);
            });
        }

        return arr;
    }

    createAddressFromData(obj) {
        let firstLine = '';
        let secondLine = '';

        try {

            let address = obj.ADDRESS;
            let streetNumber = address.address_components[0].short_name;
            let streetName = address.address_components[1].short_name;
            let sublocality = address.address_components[2].short_name;
            let city = address.address_components[3].short_name;
            let state = address.address_components[4].short_name;

            firstLine = streetName + ', ' + streetNumber;
            secondLine = sublocality + ', ' + city + '-' + state;

        } catch (error) {
            this.logMyErrors(error);
            //return <Text style={{ color: 'black' }}>Não foi possível obter o endereço</Text>;
            return null;
        }

        return { firstLine: firstLine, secondLine: secondLine };
    }

    renderSellPointsAddresses() {
        if (this.state.data && this.state.data.length !== 0) {
            return this.state.data.map((obj, index) => {

                let firstLine = '';
                let secondLine = '';

                try {

                    let address = obj.ADDRESS;
                    let streetNumber = address.address_components[0].short_name;
                    let streetName = address.address_components[1].short_name;
                    let sublocality = address.address_components[2].short_name;
                    let city = address.address_components[3].short_name;
                    let state = address.address_components[4].short_name;

                    firstLine = streetName + ', ' + streetNumber;
                    secondLine = sublocality + ', ' + city + '-' + state;

                } catch (error) {
                    this.logMyErrors(error);
                    //return <Text style={{ color: 'black' }}>Não foi possível obter o endereço</Text>;
                    return null;
                }

                return (
                    <TouchableOpacityEx key={index} onPress={() => {
                        this.animateMapToRegion({
                            latitude: parseFloat(obj.GPS_LATITUDE),
                            longitude: parseFloat(obj.GPS_LONGITUDE),
                            latitudeDelta: 0.0001,
                            longitudeDelta: 0.0001
                        });
                        //this.togglePanel();
                    }}>
                        <View key={index} style={{ paddingHorizontal: 10, justifyContent: 'center', }}>
                            <Text style={{ paddingTop: 5, color: Colors.fontColor, fontSize: 15, }}>
                                {firstLine}
                            </Text>
                            <Text style={{ paddingBottom: 5, color: Colors.fontColor, fontSize: 12 }}>
                                {secondLine}
                            </Text>
                            <View style={{ width: '100%', height: 1, backgroundColor: Colors.defaultBackground, }} />
                        </View>
                    </TouchableOpacityEx>
                )
            });
        } else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ padding: 10, fontSize: 15, fontWeight: 'bold' }}>Nenhum ponto encontrado na sua região.</Text>
                </View>
            )
        }
    }

    animateMapToRegion(region) {
        this._map.animateToRegion(region, 2000);
    }

    togglePanel() {
        if (!this._panel) {
            return;
        }
        let transitionTo = 40;
        this._panel.transitionTo(transitionTo);
    }

    render() {
        if (!this.state.hideLoading) {
            return (
                <Loading hide={false} color={this.mainColor} />
            )
        }
        return (
            <View style={{ flex: 1, paddingTop: this.state.statusBarHeight, backgroundColor: 'white' }}>
                <MapView
                    ref={c => (this._map = c)}
                    provider={PROVIDER_GOOGLE}
                    style={{ flex: 1 }}
                    showsUserLocation
                    onMapReady={() => { if (this.coords) this.animateMapToRegion(this.coords) }}
                    showsMyLocationButton>
                    {this.renderMarkers()}
                </MapView>
                <Loading loadColor={this.mainColor} defaultText hide={this.state.hideLoading} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        zIndex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    panel: {
        flex: 1,
        backgroundColor: 'white',
    },
    panelHeader: {
        flexDirection: 'row',
        height: 40,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
});