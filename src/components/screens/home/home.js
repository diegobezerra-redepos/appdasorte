
import React, { Component } from 'react'
import { Text, ScrollView, View, StyleSheet, TouchableOpacity, Image, TouchableHighlight } from 'react-native'
import Dimens from '../../../resources/dimens'
import Colors from '../../../resources/colors'
import AppComponent from '../../appComponent'
import NoResult from '../../noResults'
import LinearGradient from 'react-native-linear-gradient'
import Card from '../../lottery/card'
import FitImage from 'react-native-fit-image'
import AcquireCertificateButton from '../../button/acquireCertificateButton'
import PrizeContainer from './prizeContainer'
import withPreventDoubleClick from '../../../util/withPreventDoubleClick'
import { Icon } from 'react-native-elements'
import Application from '../../../appConfig/application'
import Modal from 'react-native-modalbox'
import CountDownBar from '../../countDownBar'

const TouchableHighlightEx = withPreventDoubleClick(TouchableHighlight);
const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

export default class HomeScreen extends AppComponent {

    componentDidMount() {
        super.componentDidMount();
        this.application.navigateMenuDrawerWithNotificationAction();
        this.openPopup();
    }

    openPopup() {
        if (this.params.popupmobile && this.params.popupmobile.banner) {
            this.refs.modal.open();
        }
    }

    render() {

        let bannerImg = this.params.bannermobile;
        let bannerLogo = this.params.logos;

        let bannerPopup = require('../../../resources/img/noimage.jpg');
        if (this.params.popupmobile && this.params.popupmobile.banner) {
            bannerPopup = { uri: this.params.popupmobile.banner };
        }

        if (this.isEditionValid() && bannerImg && bannerLogo) {
            return (
                <View style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                    <ScrollView contentContainerStyle={{ paddingBottom: Dimens.defaultPadding, }}>
                        <CountDownBar
                            params={this.params} />
                        <TouchableHighlightEx onPress={() => this.props.navigation.navigate('BuyCertification')}>
                            <LinearGradient colors={['white', 'white', Colors.defaultBackground]} style={{ paddingHorizontal: 10, paddingBottom: 10, }}>
                                <Card>
                                    <FitImage resizeMode= 'contain' source={{ uri: bannerImg }} />
                                    <FitImage source={{ uri: bannerLogo }} />
                                    <View style={styles.buttonContainer}>
                                        <AcquireCertificateButton navigate={this.props.navigation.dangerouslyGetParent().navigate} />
                                    </View>
                                </Card>
                            </LinearGradient>
                        </TouchableHighlightEx>
                        <View style={{ paddingHorizontal: Dimens.defaultPadding, }}>
                            <TouchableOpacityEx style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => {
                                    //this.refs.modal.open();
                                    this.props.navigation.dangerouslyGetParent().navigate('PrizeScreen');
                                }}>
                                <Text style={{ color: Colors.linkBlue, marginRight: 5 }}>Veja as premiações do sorteio</Text>
                                <Icon name='arrow-right-circle' color={Colors.linkBlue} type='simple-line-icon' />
                            </TouchableOpacityEx>
                            {/* <PrizeContainer params={this.params} width={this.dimensions.width} /> */}
                        </View>
                    </ScrollView>
                    <Modal
                        ref={'modal'}
                        style={styles.modal}>
                        <TouchableOpacity
                            style={{ width: '99%', borderRadius: 10 }}
                            onPress={() => {
                                if (this.params.popupmobile.link != undefined) {
                                    this.openUrl(this.params.popupmobile.link);
                                }
                            }}>
                            <FitImage source={bannerPopup} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.refs.modal.close()}
                            style={{ position: 'absolute', right: -8, top: -8, backgroundColor: 'white', borderRadius: 30 }}>
                            <Icon size={32} name='close-circle' color={'red'} type='material-community' />
                        </TouchableOpacity>
                    </Modal>
                </View>
            )
        } else {
            return (
                <NoResult hasEdition={this.hasEdition} />
            )
        }
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        paddingBottom: Dimens.halfDefaultPadding,
    },
    cardView: {
        margin: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        backgroundColor: 'white',
        borderColor: Colors.defaultBackground,
        borderWidth: 1,
        flex: 1
    },
    buttonContainer: {
        width: '100%',
        paddingStart: Dimens.defaultPadding,
        paddingEnd: Dimens.defaultPadding,
        paddingBottom: Dimens.defaultPadding
    },
    button: {
        backgroundColor: Colors.linkBlue,
        alignSelf: 'center',
        marginTop: Dimens.defaultPadding,
        padding: Dimens.defaultPadding,
        borderRadius: 5,
        width: '100%',
        marginHorizontal: Dimens.defaultPadding
    },
    textButton: {
        textAlign: 'center',
        color: 'white',
    },
    modal: {
        width: '90%',
        height: 300,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
