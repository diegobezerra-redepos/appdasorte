
import React, { Component } from 'react'
import { View, Text, FlatList, Image, InteractionManager } from 'react-native'
import AppComponent from '../../appComponent'
import LinearGradient from 'react-native-linear-gradient'
import Colors from '../../../resources/colors'
import Card from '../../lottery/card'
import DivisorElement from '../../lottery/divisorElement'
import LotteryCard from '../../lottery/lotteryCard'
import Lottery, { LineType } from '../buyCertification/lottery/lottery'
import Loading from '../../loading/loading'


class CertificateDetailItem extends Component {

    render() {

        let items = this.props.items;

        return items.map((cert, i) => {
            return (
                <Lottery
                    key={i}
                    showValue
                    textAlignLeft
                    lineType={LineType.TWO_LINES}
                    item={cert}
                    sequentialType={this.props.sequentialType} />
            )
        });

        // return (
        //     <View>
        //         {
        //             items.map((cert, i) => {
        //                 return <LinearGradient key={i} colors={['white', 'white', Colors.defaultBackground]} style={{ paddingHorizontal: 10, paddingBottom: 10, }}>
        //                     <Card>
        //                         {
        //                             cert.itens.map((obj, j) => {

        //                                 //let title = 'Estes são os certificados que estão pendentes (' + items.length + ')\n\nCertificado: ' + obj.numero + '\n' + 'Nº da sorte: ' + obj.sequencial + '\n' + 'Valor: R$ ' + Number.parseFloat(obj.valor).toFixed(2);
        //                                 let title = 'Certificado: ' + obj.numero + '\n' + 'Nº da sorte: ' + obj.sequencial + '\n' + 'Valor: R$ ' + Number.parseFloat(obj.valor).toFixed(2);

        //                                 return <View style={{ padding: 10 }} key={j}>
        //                                     <DivisorElement textAlignLeft
        //                                         title={title} />
        //                                     {/* <Text style={{ fontSize: 13, fontWeight: 'bold' }}>Nº da sorte: {obj.sequencial} </Text> */}
        //                                     <View style={{ marginBottom: 10 }}>
        //                                         <LotteryCard dozensSize={30} key={j} dozens={obj.dezenas.split('-')} />
        //                                     </View>
        //                                 </View>
        //                             })
        //                         }

        //                     </Card>
        //                 </LinearGradient>
        //             })
        //         }
        //     </View>
        // )
    }
}

export default class CertificatatesTransactionsDetail extends AppComponent {

    static navigationOptions = ({ navigation }) => {
        return AppComponent.checkoutScreenHeader(navigation, 'Certificados');
    }

    state = {
        data: null,
    }

    componentDidMount() {
        super.componentDidMount();

        InteractionManager.runAfterInteractions(() => {
            let lotteryDate = this.props.navigation.getParam('lotteryDate', '');
            let data = this.props.navigation.getParam('sells', []);
            let isPendingTransaction = this.props.navigation.getParam('isPendingTransaction', false);

            if (data.length > 0) {
                let arrData = [];
                arrData.push({ lotteryDate: lotteryDate, qtd: data.length, isPendingTransaction: isPendingTransaction });
                this.setState({ data: arrData.concat(data) });
            }
        });
    }

    renderItem({ item, index }) {
        if (index === 0) {

            let title = 'Estes são os certificados que foram validados';
            let textColor = 'green';

            if (item.isPendingTransaction) {
                title = 'Estes são os certificados que estão pendentes';
                textColor = 'red';
            }
            return (
                <Text style={{ paddingTop: 10, paddingHorizontal: 5, color: textColor, backgroundColor: 'white', fontSize: 15, fontWeight: 'bold', textAlign: 'center' }}>
                    {title + ' (' + item.qtd + ') Data do sorteio: ' + item.lotteryDate}
                </Text>);
        }
        return <CertificateDetailItem
            items={item.certificados}
            sequentialType={this.sequentialType} />;
    }

    render() {
        if (!this.state.data) {
            return (
                <Loading hide={false} color={this.mainColor} />
            )
        }
        return (
            <View style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                <FlatList
                    data={this.state.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderItem.bind(this)} />
            </View>
        )
    }
}