
import React, { Component } from 'react'
import { View, Animated, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import DivisorElement from '../../../lottery/divisorElement'
import Card from '../../../lottery/card'
import LotteryCard from '../../../lottery/lotteryCard'
import Colors from '../../../../resources/colors'
import Lottery, { LineType } from './lottery';

const ANIMATION_DURATION = 300;

export default class CheckoutRowItem extends Component {

    constructor(props) {
        super(props);

        this._animated = new Animated.Value(1);
    }

    getContent = (showDivisor, index, deleteAction) => {
        if (showDivisor) {
            return <DivisorElement deleteActions={deleteAction} key={index} title={'Combinação ' + (index + 1)} />
        }
        return null;
    }

    cardTitle(obj) {
        if (obj && obj.sequencial) {
            let strNumber = obj.sequencial.substring(0, 3) + '.' + obj.sequencial.substring(2, 6);
            //return 'Número do Certificado: ' + strNumber + '\nNúmero da Sorte: ' + strNumber;
            return 'Número do Certificado: ' + strNumber + '\n' + 'Valor: R$ ' + parseFloat(obj.value).toFixed(2);
        }
        return null;
    }

    onRemove = (item) => {
        const { deleteAction } = this.props;
        if (deleteAction) {
            Animated.timing(this._animated, {
                toValue: 0,
                duration: ANIMATION_DURATION,
            }).start(() => {
                deleteAction(item);
            });
        }
    };

    render() {        

        return (
            <Animated.View style={{ opacity: this._animated }}>
                <Lottery
                    lineType={LineType.TWO_LINES}
                    showValue
                    sequentialType={this.props.sequentialType}
                    item={this.props.item}
                    deleteAction={(item) => {
                        let onRe = this.onRemove.bind(this);
                        onRe(item);
                    }}>
                </Lottery>
            </Animated.View>
            // <Animated.View key={index} style={{ opacity: this._animated }}>
            //     <LinearGradient key={index} colors={['white', 'white', Colors.defaultBackground]} style={{ paddingHorizontal: 10, paddingBottom: 10, }}>
            //         <Card>
            //             <View style={{ padding: 10 }}>
            //                 <DivisorElement deleteButton deleteAction={ ()=> { this.deleteItem(this.props.deleteAction) } } title={this.cardTitle(item)} />
            //                 {item.itens.map((obj2, j) => {
            //                     return (
            //                         <View key={obj2.sequencial} style={{ marginBottom: 10 }}>
            //                             {this.getContent(item.itens.length > 1, j)}
            //                             <LotteryCard dozensSize={30} dozens={dezenas} identifier={obj2.sequencial} />
            //                         </View>)
            //                 })}                            
            //             </View>
            //         </Card>
            //     </LinearGradient>
            // </Animated.View>
        )
    }
}