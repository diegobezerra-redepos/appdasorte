import React from 'react'
import { Text, Alert, View, StyleSheet, FlatList } from 'react-native'
import AppComponent from '../../appComponent'
import Swiper from 'react-native-swiper'
import PrizeContainer from '../home/prizeContainer'
import { ScrollView } from 'react-native-gesture-handler';

export default class PrizeScreen extends AppComponent {

  static navigationOptions = ({ navigation }) => {
    return AppComponent.checkoutScreenHeader(navigation, 'Premiações');
  }

  // if (sorteios[j].tipo_sorteio == 2) {
  //     html += sorteios[j].sorteio_premiacoes[valores[i]].descricao_premio;
  //   } else {
  //     html +=
  //       sorteios[j].numero +
  //       "º - Prêmio - " +
  //       sorteios[j].sorteio_premiacoes[valores[i]].descricao_premio;
  //   }

  componentDidMount() {
    super.componentDidMount();
  }

  render() {
    return (
      <PrizeContainer params={this.params} isTabuled={this.isTabuled} mainColor={this.mainColor} />
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }
})