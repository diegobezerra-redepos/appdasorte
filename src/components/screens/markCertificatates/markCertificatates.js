import React from 'react'
import { InteractionManager, Text, Switch, StyleSheet, Picker, FlatList, View } from 'react-native'
import AppComponent from '../../appComponent'
import { NavigationEvents } from 'react-navigation';
import Loading from '../../loading/loading'
import Lottery, { LineType } from '../buyCertification/lottery/lottery'
import Colors from '../../../resources/colors'
import MarkCertificateController from '../../../data/markCertificate/markCertificateController'
import NoCertificate from '../../noCertificate'
import MobilePicker, { pickerSelectStyles } from '../../mobilePicker'

const MarkAllValue = {
    Marked: 'marked',
    Unmarkd: 'unmarked'
}

export default class MarkCertificatates extends AppComponent {

    state = {
        showInitLoading: true,
        hideLoading: true,
        selectedPrize: -1,
        selectedEdition: null,
        selectedEditionPicker: -1,
        markAll: true,
        hasData: false
    }

    onWillFocus = () => {
        this.getUserEdition();
    }

    componentDidMount() {
        super.componentDidMount();
        this.selectedDozens = {};
        this.dozensTapFunc = {};
    }

    getUserEdition() {
        this.setState({ showInitLoading: true })
        MarkCertificateController.getUserEdition(this.usertoken, this.userData.usuario.id).
            then((result) => {
                if (result.data.success === 'true') {
                    const data = result.data.data;
                    if (data && data.length > 0) {
                        this.resolveData(data);
                    } else {
                        this.setState({ hasData: false, data: null, showInitLoading: false });
                    }
                } else {
                    this.setState({ showInitLoading: false, errorMsg: true });
                }
            }).catch((error) => {
                this.logMyErrors(error);
                this.setState({ showInitLoading: false, errorMsg: true });
            });
    }

    componentWillUnmount() {
        this.dozenStates = null;
    }

    get currentMarkAllValue() {
        return this.state.markAll ? MarkAllValue.Marked : MarkAllValue.Unmarkd;
    }

    createPrizesArray(prizes, editionData) {
        prizes[editionData.id_edicao] = [];
        for (let i = 0; i < editionData.sorteios; i++) {
            let index = i + 1;
            prizes[editionData.id_edicao].push(index + 'º Prêmio');
        }
    }

    createEditionData(certificates, editionData) {
        certificates[editionData.id_edicao] = [];
        let arr = editionData.certificados;
        arr.forEach((certArr) => {
            let data = {
                sequencial: '',
                itens: []
            }
            certArr.forEach((item) => {
                data.sequencial = item.sequencial_pai;
                data.itens.push({
                    idEdition: editionData.id_edicao,
                    sequencial: item.sequencial,
                    dezenas: item.dezenas
                });
            });
            certificates[editionData.id_edicao].push(data);
        });
    }

    createSelectedDozensObj(selectedDozens, prizes, edition) {
        const editionId = edition.id_edicao;
        prizes[edition.id_edicao].forEach((prize) => {
            let editionLevel = this.initObj(selectedDozens, editionId);
            this.initObj(editionLevel, prize);
        });
    }

    initObj(obj, key) {
        if (!obj[key]) {
            obj[key] = {};
        }
        return obj[key];
    }

    resolveData(data) {

        let editionsData = [];
        let prizes = {};
        let certificatesData = {};

        data.forEach((edition, index) => {
            let dateLabel = index === 0 ? edition.data + ' - edição atual' : edition.data;
            editionsData.push({ label: dateLabel, value: edition.id_edicao });
            this.createPrizesArray(prizes, edition);
            this.createEditionData(certificatesData, edition);
            this.createSelectedDozensObj(this.selectedDozens, prizes, edition);
        });

        this.resolvedData = {
            prizes: prizes,
            certificatesData: certificatesData,
            editionsData: editionsData
        }

        const idEdition = data[0].id_edicao;
        this.pickerItems = this.createPickerItems(prizes[idEdition]);
        this.editionPickerItems = this.createPickerEdtionsItems(editionsData);
        this.oldSelectedPrize = this.pickerItems[0].value;
        this.oldSelectedEdition = idEdition;
        this.setState({
            showInitLoading: false,
            selectedPrize: prizes[idEdition][0],
            selectedEdition: idEdition,
            flatListData: this.resolvedData.certificatesData[idEdition],
            hasData: true
        });
    }

    createPickerItems(data) {
        return data.map((obj, index) => {
            return { label: obj, value: obj };
        })
    }

    createPickerEdtionsItems(data) {
        return data.map((obj, index) => {
            //return { label: obj.label, value: obj.value };
            return obj;
        })
    }

    onDozenTap(dozen, marked, certificateSequential) {

        console.log(this.dozensTapFunc);

        const selectedEdition = this.state.selectedEdition;
        const selectedPrize = this.state.selectedPrize;
        const prizeLevel = this.selectedDozens[selectedEdition][selectedPrize];
        const newMarkedVal = !marked;

        if (this.currentMarkAllValue === MarkAllValue.Marked) {
            Object.keys(this.dozensTapFunc[selectedEdition])
                .forEach((sequential) => {
                    const cert = this.dozensTapFunc[selectedEdition][sequential];
                    if (cert[dozen]) {
                        this.updateSelectedDozens(newMarkedVal, prizeLevel, sequential, dozen);
                        cert[dozen](newMarkedVal);
                    }
                });
        } else {
            const dozendFunc = this.dozensTapFunc[selectedEdition][certificateSequential][dozen];
            dozendFunc(newMarkedVal);
            this.updateSelectedDozens(newMarkedVal, prizeLevel, certificateSequential, dozen);
        }

    }

    updateSelectedDozens(newMarkedVal, prizeLevel, sequential, dozen) {
        if (newMarkedVal) {
            if (!prizeLevel[sequential]) {
                prizeLevel[sequential] = [];
            }
            prizeLevel[sequential].push(dozen);
        } else {
            if (prizeLevel[sequential]) {
                const index = prizeLevel[sequential].findIndex((selectedDozen) => selectedDozen === dozen);
                if (index !== -1) {
                    prizeLevel[sequential].splice(index, 1);
                    if (prizeLevel[sequential].length === 0) {
                        delete prizeLevel[sequential];
                    }
                }
            }
        }
    }

    onEditionSelected(selectedEdition) {
        const oldSelectedEdtion = this.oldSelectedEdition;
        this.setState({
            hideLoading: false,
        }, () => {
            setTimeout(() => {
                this.setState({
                    selectedEdition: selectedEdition,
                },
                    () => this.onEditionChange(selectedEdition, oldSelectedEdtion))
            }, 100);
        });
    }

    onEditionChange(selectedEdition, oldSelectedEdtion) {

        const selectedPrize = this.state.selectedPrize;
        let oldPrizeLevel = this.selectedDozens[oldSelectedEdtion][selectedPrize];
        let prizeLevel = this.selectedDozens[selectedEdition][selectedPrize];

        Object.keys(oldPrizeLevel)
            .forEach(async (sequential) => {
                //renmove as dezenas da edição antiga
                oldPrizeLevel[sequential].forEach(async (dozen) => {
                    if (this.dozensTapFunc[oldSelectedEdtion][sequential][dozen]) {
                        await this.dozensTapFunc[oldSelectedEdtion][sequential][dozen](false);
                    }
                });
            });

        Object.keys(prizeLevel)
            .forEach(async (sequential) => {
                //adiciona as dezenas do prêmio novo
                prizeLevel[sequential].forEach(async (dozen) => {
                    if (this.dozensTapFunc[selectedEdition][sequential][dozen]) {
                        await this.dozensTapFunc[selectedEdition][sequential][dozen](true);
                    }
                });
            });

        this.setState({ hideLoading: true });
    }

    onPrizeSelected(selectedPrize) {

        const oldSelectedPrize = this.oldSelectedPrize;
        const idEdition = this.state.selectedEdition;

        this.setState({ hideLoading: false, }, () => {
            setTimeout(() => {
                let oldPrizeLevel = this.selectedDozens[idEdition][oldSelectedPrize];
                let prizeLevel = this.selectedDozens[idEdition][selectedPrize];

                Object.keys(oldPrizeLevel)
                    .forEach(async (sequential) => {
                        //remove as dezenas do prêmio antigo
                        oldPrizeLevel[sequential].forEach(async (dozen) => {
                            this.dozensTapFunc[idEdition][sequential][dozen] && await this.dozensTapFunc[idEdition][sequential][dozen](false);
                        });
                    });

                Object.keys(prizeLevel)
                    .forEach(async (sequential) => {
                        //adiciona as dezenas do prêmio novo
                        prizeLevel[sequential].forEach(async (dozen) => {
                            this.dozensTapFunc[idEdition][sequential][dozen] && await this.dozensTapFunc[idEdition][sequential][dozen](true);
                        });
                    });

                setTimeout(() => {
                    this.setState({ hideLoading: true });
                }, 300);
            }, 100);
        });
    }

    onCreatingDozen(dozenTap, dozen, certificateSequential, editionId) {
        let editionLevel = this.initObj(this.dozensTapFunc, this.state.selectedEdition);
        let sequentialLevel = this.initObj(editionLevel, certificateSequential);
        sequentialLevel[dozen] = dozenTap;
    }

    onUmountDozen(dozen, certificateSequential, editionId) {
        delete this.dozensTapFunc[editionId][certificateSequential][dozen];
    }

    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item, index }) => {
        return (
            <Lottery
                key={index}
                onCreatingDozen={this.onCreatingDozen.bind(this)}
                onUmountDozen={this.onUmountDozen.bind(this)}
                onDozenTap={this.onDozenTap.bind(this)}
                lineType={LineType.GRID}
                markedDozens={[]}
                item={item} />
        )
    }

    render() {
        if (this.state.showInitLoading) {
            return (
                <View style={{ flex: 1 }}>
                    <NavigationEvents
                        onWillFocus={() => { this.onWillFocus() }} />
                    <Loading defaultText hide={false} loadColor={this.mainColor} />
                </View>
            )
        }
        if (!this.state.hasData) {
            return (
                <View style={{ flex: 1 }}>
                    <NavigationEvents
                        onWillFocus={() => { this.onWillFocus() }} />
                    <NoCertificate text='Você não possui certificados.' navigation={this.props.navigation} />
                </View>
            )
        }
        return (
            <View style={{ flex: 1, backgroundColor: Colors.defaultBackground }}>
                <NavigationEvents
                    onWillFocus={() => { this.onWillFocus() }} />
                <View style={{ justifyContent: 'center', backgroundColor: '#F0F0F0' }}>
                    <View style={styles.pickerStyle}>
                        <MobilePicker
                            style={pickerSelectStyles}
                            items={this.pickerItems}
                            value={this.state.selectedPrize}
                            fontColor={'black'}
                            onValueChange={(itemValue, index, callback) => {
                                this.setState({
                                    selectedPrize: itemValue
                                }, () => callback());
                            }}
                            loadResults={(itemValue) => {
                                this.onPrizeSelected(itemValue);
                                this.oldSelectedPrize = itemValue;
                            }}
                        />
                    </View>
                    <View style={{ paddingHorizontal: 10, paddingVertical: 5, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', width: 100, alignItems: 'center', marginRight: 0 }}>
                            <Text
                                style={{
                                    fontSize: 13,
                                    fontWeight: 'bold',
                                    marginRight: 5
                                }}>
                                Marcar simultâneo:
                                </Text>
                            <Switch
                                value={this.state.markAll}
                                onValueChange={(val) => {
                                    this.setState({ markAll: val });
                                }} />
                        </View>
                        <View style={{ flex: 1, marginLeft: 40 }}>
                            <MobilePicker
                                style={pickerSelectEditionStyles}
                                items={this.editionPickerItems}
                                value={this.state.selectedEditionPicker}
                                fontColor={Colors.fontColor}
                                onValueChange={(itemValue, index, callback) => {
                                    this.setState({
                                        selectedEditionPicker: itemValue
                                    }, () => callback());
                                }}
                                loadResults={(itemValue) => {
                                    this.onEditionSelected(itemValue);
                                    this.oldSelectedEdition = itemValue;
                                }}
                            />
                        </View>
                    </View>
                </View>
                <View style={{ width: '100%', height: 1, backgroundColor: Colors.defaultBackground, marginBottom: 0 }} />
                <FlatList
                    initialNumToRender={100}
                    data={this.resolvedData.certificatesData[this.state.selectedEdition]}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem.bind(this)} />
                <Loading hide={this.state.hideLoading} loadColor={this.mainColor} />
            </View >
        )
    }
}

const styles = StyleSheet.create({
    pickerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginTop: 10,
        borderWidth: 1,
        height: 40,
        borderColor: Colors.divisor
    }
});

const pickerSelectEditionStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 13,
        fontWeight: 'bold',
        paddingVertical: 12,
        paddingLeft: 5,
        color: Colors.fontColor,
        paddingRight: 30 // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 13,
        fontWeight: 'bold',
        paddingVertical: 12,
        marginLeft: 40,
        paddingLeft: 5,
        color: Colors.fontColor,
        paddingRight: 30 // to ensure the text is never behind the icon
    }
});

const json = {
    "success": "true",
    "data": [
        {
            "id_edicao": "3087",
            "numero_pai": "104.0.100.201-85",
            "sequencial_pai": "100201",
            "data": "14/04/2019",
            "imagem": "http://mobile-dev.redepos.com.br/index.php/../app/img/edicao/3087.jpg?1554320364",
            "imagemmobile": "http://mobile-dev.redepos.com.br/index.php/../app/img/edicao/3087-mobile.jpg?1554320364",
            "certificados": [
                [
                    {
                        "sequencial_pai": "100223",
                        "numero_pai": "104.0.100.223-18",
                        "sequencial": "200445",
                        "numero": "104.0.200.445-67",
                        "dezenas": "02-03-04-05-13-19-23-24-36-37-38-39-40-42-43-47-53-54-55-60"
                    },
                    {
                        "sequencial_pai": "100223",
                        "numero_pai": "104.0.100.223-18",
                        "sequencial": "200446",
                        "numero": "104.0.200.446-20",
                        "dezenas": "03-06-07-08-17-18-24-27-28-29-30-33-40-43-44-45-47-53-58-59"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200402",
                        "numero": "104.0.200.402-54",
                        "dezenas": "01-09-15-17-18-19-22-23-28-29-31-38-40-42-44-46-50-55-58-60"
                    },
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200401",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200502",
                        "numero": "104.0.200.402-54",
                        "dezenas": "01-09-15-17-18-19-22-23-28-29-31-38-40-42-44-46-50-55-58-60"
                    },
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200501",
                        "numero": "104.0.200.402-54",
                        "dezenas": "01-09-15-17-18-19-22-23-28-29-31-38-40-42-44-46-50-55-58-60"
                    },
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200401",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200601",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200602",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200701",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200702",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
                [
                    {
                        "sequencial_pai": "100201",
                        "numero_pai": "104.0.100.201-85",
                        "sequencial": "200801",
                        "numero": "104.0.200.401-91",
                        "dezenas": "09-12-13-14-19-20-21-24-33-34-35-38-39-41-45-48-50-57-59-60"
                    }
                ],
            ],
            "sorteios": "4"
        },
        {
            "id_edicao": "3067",
            "numero_pai": "090.0.100.040-01",
            "sequencial_pai": "100040",
            "data": "31/03/2019",
            "imagem": "http://mobile-dev.redepos.com.br/index.php/../app/img/edicao/3067.jpg?1554320364",
            "imagemmobile": "http://mobile-dev.redepos.com.br/index.php/../app/img/edicao/3067-mobile.jpg?1554320364",
            "certificados": [
                [
                    {
                        "sequencial_pai": "100040",
                        "numero_pai": "090.0.100.040-01",
                        "sequencial": "200080",
                        "numero": "090.0.200.080-43",
                        "dezenas": "05-07-10-17-18-20-21-25-27-28-32-35-40-45-47-49-53-56-57-58"
                    },
                    {
                        "sequencial_pai": "100040",
                        "numero_pai": "090.0.100.040-01",
                        "sequencial": "200079",
                        "numero": "090.0.200.079-64",
                        "dezenas": "03-04-05-10-16-20-21-22-27-33-36-39-40-43-46-49-52-55-57-58"
                    }
                ]
            ],
            "sorteios": "4"
        }
    ]
}