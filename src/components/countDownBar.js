import React, { Component } from 'react'
import { Text, View, Animated, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from '../resources/colors'
import CountDown from 'react-native-countdown-component';
import { Icon } from 'react-native-elements'
import moment from 'moment';

export default class CountDownBar extends Component {

    constructor(props) {
        super(props);
        let show = props.params.info.edicao_atual.mostrar_contagem != null ? props.params.info.edicao_atual.mostrar_contagem : true;
        let endDate = props.params.info.edicao_atual.data_hora_fim;
        let nowServer = props.params.info.edicao_atual.data_hora_atual ?
            new moment(props.params.info.edicao_atual.data_hora_atual).valueOf() : new moment().valueOf();
            nowServer = new moment(props.params.info.edicao_atual.data_hora_atual).valueOf();

        let countDownDate = moment(endDate).valueOf();
        let now = new moment().valueOf();
        let distanceFromServerNow = (countDownDate - nowServer);
        let diffNow = (nowServer - now);
        let dateFuture = new moment(now + distanceFromServerNow + diffNow).valueOf();
        let distance = (dateFuture - now) / 1000;

        this.state = {
            countdownFinished: false,
            until: distance,
            show: show
        }
    }

    render() {

        let backgroundColor = this.props.params.css.strCorTextoTopo;
        let mainColor = this.props.params.css.strCorBarra;
        let title = this.props.params.info.edicao_atual.titulo_contador ? this.props.params.info.edicao_atual.titulo_contador : "Encerramento:";

        if (this.state.show && this.state.until > 0 && !this.state.countdownFinished) {
            return (
                <View
                    style={[styles.main,
                    { backgroundColor: mainColor }]}>
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-end', alignItems: 'center' }}>
                        <Text style={[styles.title, { width: '100%', color: mainColor, backgroundColor: backgroundColor }]}>{title}</Text>
                    </View>
                    <View style={styles.subContainer}>
                        <View style={styles.iconContainer}>
                            <Icon name='clock' size={35} color={backgroundColor} type='material-community' />
                        </View>
                        <CountDown
                            size={15}
                            digitTxtStyle={{ color: mainColor }}
                            digitStyle={{ backgroundColor: backgroundColor }}
                            timeLabelStyle={{ color: backgroundColor, fontWeight: 'bold' }}
                            until={this.state.until}
                            timeLabels={{ d: 'Dia', h: 'Hr', m: 'Min', s: 'Seg' }}
                            onFinish={() => this.setState({ countdownFinished: true })}
                        />
                    </View>
                </View>
            )
        } else {
            return null;
            // <View
            //     style={[styles.main,
            //     { backgroundColor: mainColor } ]}>
            //     <Text style={[styles.title, { width: '100%', color: mainColor, backgroundColor: backgroundColor }]}>Tempo para participação do sorteio encerrado!</Text>
            // </View>
        }
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        margin: 8,
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 3
    },
    title: {
        textAlign: 'center',
        padding: 5,
        fontWeight: 'bold',
        fontSize: 16
    },
    subContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 8
    },
    iconContainer: {
        marginEnd: 5,
        marginTop: 2
    }
});