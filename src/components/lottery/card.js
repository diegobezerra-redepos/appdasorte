
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'

export default class Card extends Component {
    render() {
        return (
            <View style={[styles.main, this.props.style]}>
                {this.props.children}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        margin: 10,
        width: '100%',
        alignSelf: 'center',
        backgroundColor: 'white',
        //borderRadius: 5,
        elevation: 0.5
    }
});