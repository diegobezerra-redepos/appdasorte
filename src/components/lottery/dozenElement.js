
import React, { Component } from 'react'
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native'
import Colors from '../../resources/colors'
import LinearGradient from 'react-native-linear-gradient'

const DEFAULT_SIZE = 50;

export default class DozenElement extends Component {

    constructor() {
        super();
        this.state = {
            initializate: true
        };
    }

    componentDidMount() {
        if (this.props.markedState) {
            let setState = this.setMarkedState.bind(this);
            this.props.markedState.addDozenState(this.props.dozen, this.props.certificateNumber, setState);
        }
    }

    componentWillUnmount() {
        if (this.props.markedState) {
            this.props.markedState.removeDozenStateFunction(this.props.dozen, this.props.certificateNumber);
        }
    }

    setMarkedState(state) {
        let newState = state ? state : !this.state.marked;
        this.setState({ marked: newState });
    }

    static getDerivedStateFromProps(props, state) {

        if (state.initializate) {
            let width = props.size ? props.size : DEFAULT_SIZE,
                state = {
                    initializate: false,
                    width: width,
                    height: props.size ? props.size - 5 : DEFAULT_SIZE - 5,
                    fontSize: width / 2,
                    markedStyle: props.marked ? styles.markedStyle : {},
                    marked: null,
                }

            return state;
        }

        state.markedStyle = state.marked ? styles.markedStyle : {};

        return state;
    }

    render() {

        if (this.props.markedState) {
            return (
                <TouchableOpacity
                    onPress={() => {
                        this.props.markedState.setDozenState(this.props.dozen, this.props.certificateNumber);
                    }}>
                    <LinearGradient style={[{ width: this.state.width, height: this.state.height }, styles.numberView]} colors={['white', 'white', Colors.defaultBackground]}>
                        <Text style={[{ fontSize: this.state.fontSize }, styles.numberText]}>{this.props.dozen}</Text>
                    </LinearGradient>
                    <View style={this.state.markedStyle} />
                </TouchableOpacity>
            )
        }

        return (
            <View>
                <LinearGradient style={[{ width: this.state.width, height: this.state.height }, styles.numberView]} colors={['white', 'white', Colors.defaultBackground]}>
                    <Text style={[{ fontSize: this.state.fontSize }, styles.numberText]}>{this.props.dozen}</Text>
                </LinearGradient>
                <View style={this.state.markedStyle} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    numberView: {
        //borderWidth: 1,
        borderRadius: 10,
        borderColor: Colors.blackTransparent,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        margin: 2.5
    },
    markedStyle: {
        backgroundColor: '#007AFF63',
        position: 'absolute',
        top: 0,
        margin: 2.5,
        bottom: 0,
        left: 0,
        right: 0,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
    },
    numberText: {
        fontWeight: 'bold',
    }
});