
import React, { PureComponent } from 'react'
import { View, Text } from 'react-native'
import BoxButton from './button/boxButton'
import Colors from  '../resources/colors'
import AcquireCertificateButton from './button/acquireCertificateButton';

export default class NoCertificate extends PureComponent {
    render() {

        let nav = this.props.navigation;
        let text = this.props.text

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', padding: 10 }}>{text}</Text>
                <AcquireCertificateButton navigate={nav.dangerouslyGetParent().navigate} />
            </View>
        )
    }
}