import React, { Component } from 'react'
import { StyleSheet, Platform, View } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { Icon } from 'react-native-elements'
import Colors from '../resources/colors'

const defaultProps = {
    placeholder: {},
    doneText: 'OK',
}

export default class MobilePicker extends Component {

    render() {
        if (Platform.OS === 'ios') {
            const fontColor = this.props.style.inputIOS.color;
            return (
                <View style={{ width: '100%' }}>
                    <RNPickerSelect
                        {...defaultProps}
                        style={this.props.style}
                        items={this.props.items}
                        Icon={() => <View style={{ height: 45, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='menu-down' color={fontColor} type='material-community' />
                        </View>}
                        onValueChange={(itemValue, index) => this.props.onValueChange(itemValue, index, () => { })}
                        onOpen={() => {
                            this.oldSelectedItem = this.props.value;
                        }}
                        onClose={() => {
                            if (this.props.loadResults && this.oldSelectedItem !== this.props.value) {
                                this.props.loadResults(this.props.value)
                            }
                        }}
                    />
                </View>
            )
        } else {
            const fontColor = this.props.style.inputAndroid.color;
            return (
                <RNPickerSelect
                    style={this.props.style}
                    {...defaultProps}
                    items={this.props.items}
                    Icon={() => <View style={{ height: 45, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='menu-down' color={fontColor} type='material-community' />
                    </View>}
                    onValueChange={(itemValue, index) => {
                        this.props.onValueChange(itemValue, index, () => {
                            this.props.loadResults(itemValue)
                        });
                    }}
                />
            )
        }
    }
}

export const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingVertical: 12,
        paddingHorizontal: 10,
        width: '100%',
        color: Colors.fontColor,
        paddingRight: 30 // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingVertical: 12,
        paddingHorizontal: 10,        
        width: '100%',
        color: Colors.fontColor,
        paddingRight: 30 // to ensure the text is never behind the icon
    }
});