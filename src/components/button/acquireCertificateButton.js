import React, { Component } from 'react';
import { Linking, Platform, View } from 'react-native'
import BoxButton from './boxButton'
import Colors from '../../resources/colors';

export default class AcquireCertificateButton extends Component {

    render() {
        return (
            <BoxButton
                style={{ width: '90%' }}
                text='COMPRAR CERTIFICADO'
                color={Colors.linkBlue} action={() => {
                    this.props.navigate('BuyCertification');
                }} />
        )
    }
}
