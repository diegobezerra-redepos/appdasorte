
import { Platform, NativeModules } from 'react-native'
import AndroidFlavor from '../native/androidFlavor'
import Config from 'react-native-config'
import Configurations from './configurations'
import ParameterController from '../data/parameters/parameterController'
import UserController from '../data/user/userController'
import CertificateController from '../data/certficate/certificateController'
import GeolocationInfo from "../util/geolocation"
import ServiceApi, { TOKEN_EXPIRED_CODE } from '../services/serviceApi'
import BadgeNotificationsController from '../data/badgeNotifications/badgeNotificationsController'
import FabricLogs, { FABRIC_NOTIFICATION_TYPE } from '../util/fabricLogs'
import constants from '../services/constants'


export const FABRIC_NOTIFICATION = FABRIC_NOTIFICATION_TYPE;
export const AuditType = {
    Aplicap: 'aplicap',
    Aplub: 'aplub'
}

export default class Application {

    __instance;
    __flavor;
    __checkoutBadge = 0;
    __config = {
        css: {
            strCorBarra: 'gray'
        }
    };
    __parameters;
    __userData;

    constructor() {
        this.serviceApi = ServiceApi.instance;
    }

    static get instance() {
        if (!this.__instance) {
            this.__instance = new Application();
        }
        return this.__instance;
    }

    get TokenExpiredCode() {
        return TOKEN_EXPIRED_CODE;
    }

    get configurations() {
        return this.__config;
    }

    get parameters() {
        return this.__parameters;
    }

    get userData() {
        return this.__userData;
    }

    get checkoutBadge() {
        return this.__checkoutBadge;
    }

    get coords() {
        return this.__coords;
    }

    get finalizeSellUrl() {
        console.log(this.__parameters)
        if (this.__parameters.payment && this.__parameters.payment === 'AIXMOBIL') {
            return constants.URL_AIX_FINALIZAR_VENDA;
        }
        return constants.URL_FINALIZAR_VENDA;
    }

    async onUpdateUserData(updatedUserData) {
        this.__userData.usuario = updatedUserData;
        await UserController.storeUserData(this.__userData);
        if (this.onLogin) {
            this.onLogin(this.__userData);
        }
    }

    get isProd() {
        return true;
    }

    login(userData, dontNavigate) {
        this.__userData = userData;
        if (this.onLogin) {
            this.onLogin(userData, dontNavigate);
        }
    }

    logout() {
        if (this.onLogout) {
            this.onLogout();
        }
    }

    removeUserData() {
        this.__userData = null;
        UserController.removeUserData();
    }

    async initApp() {
        this.getCoordinates();
        let oldEditionId = await ParameterController.getEditionId();
        this.checkEdition(this.__parameters, oldEditionId);
        this.initChekcoutNumber();
        await this.loadUserData();
        console.log(this.__userData);
    }

    async initChekcoutNumber() {
        let checkoutNumber = await CertificateController.retrieveSeletedCertificatesCount();
        this.setCheckoutNumber(checkoutNumber);
    }

    async loadUserData() {
        if (!this.__userData) {
            this.__userData = await UserController.retrieveUsersData();
        }
        return this.__userData;
    }

    async checkEdition(newParams, oldEditionId) {
        if (oldEditionId && newParams && newParams.info && newParams.info.edicao_atual && newParams.info.edicao_atual.id !== oldEditionId) {
            await ParameterController.removeAllData();
        }
    }

    async removeAllCertificatesData() {
        await ParameterController.removeAllData();
        this.setCheckoutNumber(0);
    }

    async getCoordinates() {
        return GeolocationInfo.getCurrentPosition().then((result) => {
            if (result) {
                this.__coords = result.coords;
            }
            return this.__coords;
        }).catch((error) => {
            console.log(error.message);
            Application.logError(error);
        });
    }

    setCheckoutNumber(number) {
        this.__checkoutBadge = number;
        if (this.setBadge) {
            this.setBadge(number);
        }
    }

    get badgeFuncs() {
        if (!this._badgeFuncs) {
            this._badgeFuncs = {};
        }
        return this._badgeFuncs;
    }

    addBadgeFunc(key, func) {
        this.badgeFuncs[key] = func;
    }

    removeBadgeFunc(key) {
        delete this.badgeFuncs[key];
    }

    setBadge(newBadgeVal) {
        Object.values(this.badgeFuncs).forEach((func) => {
            func(newBadgeVal);
        });
    }

    loadFlavor = async () => {
        if (Platform.OS === 'ios') {
            return new Promise(async (resolve, reject) => {
                const TargetConfiguration = NativeModules.TargetConfiguration
                const target = await TargetConfiguration.getTargetConfiguration();
                resolve(target);
            });
        } else {
            return new Promise((resolve, reject) => {
                AndroidFlavor.getCurrentFlavor((flavor) => {
                    console.log(flavor);
                    resolve(flavor);
                });
            });
        }
    }

    async loadParameters() {
        this.__parameters = await ParameterController.retrieveParametersData(this.__config.base);
    }

    async loadConfiguration() {

        if (!this.__flavor) {
            this.__flavor = await this.loadFlavor();
        }
        this.__config = Configurations[this.__flavor];
        this.__config = Object.assign({}, this.__config, { baseURL: constants.API_URL });
        return this.__config;
    }

    setDrawerNavigation(drawerNavigation) {
        this.drawerNavigation = drawerNavigation;
    }

    navigateMenuDrawerWithRoute(routeKey) {
        if (this.drawerNavigation) {
            //alert(JSON.stringify(this.drawerNavigation));
            this.drawerNavigation.navigate(routeKey);
        }
    }

    navigateMenuDrawerWithNotificationAction() {
        let notificationAction = BadgeNotificationsController.getNotificationRouteAction();
        if (notificationAction) {
            this.drawerNavigation.navigate(notificationAction);
            BadgeNotificationsController.setNotificationRouteAction(null);
        }
    }

    static logError(error) {
        if (error && error.message) {
            let userData = this.userData;
            FabricLogs.logError(error, userData, Platform);
        }
    }

    static logFabricPurchase(total, success, itemType, itemName, itemId, userData) {
        FabricLogs.logPurchase(
            total,
            success,
            itemType,
            itemName,
            itemId,
            userData);
    }

    static logContentView(name, userData) {
        FabricLogs.logContentView(name, userData);
    }

    static logSignUp(value) {
        FabricLogs.logSignUp(value);
    }

    static logLogin(value, userData) {
        FabricLogs.logLogin(value, userData);
    }

    static logNotification(type, data) {
        FabricLogs.logNotification(type, data);
    }

    static logAddToCart(value, itemType, itemName, itemId, userData) {
        FabricLogs.logAddToCart(value, itemType, itemName, itemId, userData);
    }

    static setFabricUser(userData) {
        FabricLogs.setUser(userData);
    }

    static crashTest() {
        FabricLogs.crashTest();
    }
}