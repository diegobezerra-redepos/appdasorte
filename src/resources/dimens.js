

const defaultPadding = 10
const halfDefaultPadding = 5
const defaultRadius = 5
const iconPadding = 30
const defaultToolBarHeight = 56

export default class Dimens {

    static get defaultPadding() {
        return defaultPadding;
    }

    static get halfDefaultPadding() {
        return halfDefaultPadding;
    }

    static get iconPadding() {
        return iconPadding;
    }

    static get defaultToolBarHeight() {
        return defaultToolBarHeight;
    }

    static get defaultRadius() {
        return defaultRadius;
    }
}