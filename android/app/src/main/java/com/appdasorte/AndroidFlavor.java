package com.appdasorte;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class AndroidFlavor extends ReactContextBaseJavaModule {

    public AndroidFlavor(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AndroidFlavor";
    }

    @ReactMethod
    public void getCurrentFlavor(Callback successCallback) {
        successCallback.invoke(BuildConfig.FLAVOR);
    }
}
